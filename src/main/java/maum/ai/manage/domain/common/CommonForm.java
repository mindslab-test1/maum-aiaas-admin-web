package maum.ai.manage.domain.common;

import lombok.Data;

/**
 * 화면에서 사용하는 공통 Form 모델
 *
 * @author unongko
 * @version 1.0
 */

@Data
public class CommonForm {

	private String function_name;
	private Integer current_page_no;
	private Integer count_per_page;
	private Integer count_per_list;
	private Integer total_page_count;
	private Integer total_list_count;
	private Integer limit;
	private Integer offset;
	private String viewType;

	private Integer CreateUser;
	private String CreateDate;
	private Integer UpdateUser;
	private String UpdateDate;
}
