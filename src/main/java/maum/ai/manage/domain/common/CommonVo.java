package maum.ai.manage.domain.common;

import lombok.Data;

/**
 * 데이터베이스에서 사용하는 공통 모델
 *
 * @author unongko
 * @version 1.0
 */

@Data
public class CommonVo {
	private Integer num;
	private Integer CreateUser;
	private String CreateUserNm;
	private String CreateDate;
	private Integer UpdateUser;
	private String UpdateUserNm;
	private String UpdateDate;

	private String createYN;
	private String updateYN;
	private String deleteYN;
}
