package maum.ai.manage.domain.common;

import lombok.Getter;
import lombok.Setter;
/**
 * 프러퍼티에서 사용하는 공통 모델
 *
 * @author unongko
 * @version 1.0
 */
@Getter
@Setter
public class PropertyItemVo {
    private Integer number;
    private String text;
}
