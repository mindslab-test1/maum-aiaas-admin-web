package maum.ai.manage.domain.common;

import lombok.Getter;

@Getter
public enum MenuAuthRoleEnum {

    R("조회"),
    RU("조회수정"),
    RD("조회삭제"),
    RUD("조회수정삭제"),
    CR("등록조회"),
    CRD("등록조회삭제"),
    CRU("등록조회수정"),
    CRUD("등록조회수정삭제");

    private String codeNm;

    MenuAuthRoleEnum(String codeNm) {
        this.codeNm = codeNm;
    }
}
