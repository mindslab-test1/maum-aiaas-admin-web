package maum.ai.manage.domain.payment;

import lombok.Data;

@Data
public class PayCsvForm {
    private String num;
    private String name;
    private String email;
    private String phone;
    private String company;
    private String payment;
    private String productName;
    private String price;
    private String currency;
    private String payDate;
    private String cancelledDate;
    private String cardName;
    private String status;
    private String payCount;



    public void makeTableHead(){
        setNum("No.");
        setName("Name");
        setEmail("Email");
        setPhone("Phone");
        setCompany("Company");
        setPayment("Payment");
        setProductName("상품명");
        setPrice("요청 금액");
        setCurrency("통화 구분");
        setPayDate("승인 일자");
        setCancelledDate("취소 일자");
        setCardName("카드 계열");
        setStatus("거래 상태");
        setPayCount("회차");
    }


}

