package maum.ai.manage.domain.payment;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonForm;

@Getter
@Setter
public class PayForm extends CommonForm {
    private String payment;
    private String payDayStart;
    private String payDayEnd;
    private String status;
    private String payCount;
    private String payCountRange;
    private String name;
    private String email;
    private String company;
    private String currentDate;
    private String startDate;
    private String endDate;
}
