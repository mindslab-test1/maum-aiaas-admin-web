package maum.ai.manage.domain.payment;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.entity.Pay;

@Getter
@Setter
public class PayDto extends Pay {
    private String day;
    private int updown;
    private int MAU;
    private int dif;

    private String name;
    private String email;
    private String phone;
    private String company;
    private String cardName;
    private int active;
    private String paymentDate;
    private int todayPay;
    private int monthPay;
    private int allPay;
    private String currency;
    private String productName;
    private int payCount;
    private String FailReason;
    private int canCancel;
}
