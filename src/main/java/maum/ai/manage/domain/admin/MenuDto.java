package maum.ai.manage.domain.admin;

import maum.ai.manage.entity.Menu;

/**
 * Menu DTO 모델
 *
 * @author unongko
 * @version 1.0
 */

public class MenuDto extends Menu {

	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
