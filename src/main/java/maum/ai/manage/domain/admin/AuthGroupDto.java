package maum.ai.manage.domain.admin;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.entity.AuthGroup;

/**
 * 권한그룹 DTO 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AuthGroupDto extends AuthGroup {

	private int subCnt;
	private String result;
}
