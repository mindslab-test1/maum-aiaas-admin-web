package maum.ai.manage.domain.admin;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonForm;

import java.util.List;

/**
 * 화면에서 사용하는 메뉴마스터 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class MenuMasterForm extends CommonForm {

	private Integer MenuMasterNo;
	private String MenuMasterType;
	private String MenuMasterNm;
	private String MenuMasterUrl;
	private String MenuMasterUseYN;
	private Integer MenuMasterOrder;
	private String LangType;
	private String IconNm;

	private String search_type;
	private String search_keyword;

	private Integer menuMasterNoArr[];
}
