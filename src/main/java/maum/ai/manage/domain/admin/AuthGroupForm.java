package maum.ai.manage.domain.admin;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonForm;

/**
 * 화면에서 사용하는 권한그룹 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AuthGroupForm extends CommonForm {

	private Integer AuthGroupNo;
	private String AuthGroupNm;
	private String Description;

	private String search_type;
	private String search_keyword;

	private Integer authGroupNoArr[];
}
