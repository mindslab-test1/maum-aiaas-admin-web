package maum.ai.manage.domain.admin;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonForm;

/**
 * 화면에서 사용하는 관리사용자 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AdminUserForm extends CommonForm {

	private Integer UserNo;
	private String UserId;
	private String Password;
	private String UserNm;
	private String Company;
	private String Dept;
	private String Phone;
	private String Email;
	private String Active;
	private Integer Enabled;
	private String Authority;
	private Integer LoginFailCNT;
	private Integer AuthGroupNo;

	private String search_type;
	private String search_keyword;

	private Integer adminUserNoArr[];
}
