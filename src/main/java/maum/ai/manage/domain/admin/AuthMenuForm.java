package maum.ai.manage.domain.admin;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonForm;

/**
 * 화면에서 사용하는 권한메뉴 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AuthMenuForm extends CommonForm {

	private Integer AuthMenuNo;
	private Integer AuthGroupNo;
	private Integer MenuMasterNo;
	private Integer MenuNo;
	private String RoleCode;

	private String MenuMasterNm;
	private String MenuNm;

	private String search_type;
	private String search_keyword;

	private Integer authMenuNoArr[];
}
