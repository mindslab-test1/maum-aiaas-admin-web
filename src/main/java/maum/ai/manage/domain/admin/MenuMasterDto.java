package maum.ai.manage.domain.admin;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.admin.MenuDto;
import maum.ai.manage.entity.MenuMaster;

import java.util.List;

/**
 * MenuMaster DTO 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class MenuMasterDto extends MenuMaster {

	private List<MenuDto> menuSubList;
	private int subCnt;
	private String result;
}
