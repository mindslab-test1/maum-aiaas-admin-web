package maum.ai.manage.domain.admin;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonForm;

/**
 * 화면에서 사용하는 메뉴 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class MenuForm extends CommonForm {

	private Integer MenuNo;
	private Integer MenuMasterNo;
	private String MenuNm;
	private String MenuUrl;
	private String MenuUseYN;
	private Integer MenuOrder;

	private String search_type;
	private String search_keyword;

	private Integer menuNoArr[];
}
