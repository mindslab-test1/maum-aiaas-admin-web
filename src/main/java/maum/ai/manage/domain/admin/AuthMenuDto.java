package maum.ai.manage.domain.admin;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.entity.AuthMenu;

/**
 * 권한메뉴 DTO 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AuthMenuDto extends AuthMenu {

	private String menuMasterNm;
	private String menuNm;
	private String roleCodeNm;
	private String result;
}
