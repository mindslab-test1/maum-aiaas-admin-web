package maum.ai.manage.domain.admin;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.entity.AdminUser;

/**
 * 사용자 DTO 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AdminUserDto extends AdminUser {

	private String result;
	private String AuthGroupNm;
}
