package maum.ai.manage.domain.user;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.entity.User;

/**
 * 사용자 DTO 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class UserDto extends User {

	private String Payment;
	private String dateFrom;
	private String dateTo;
	private String PaymentDate;

	private String result;

}
