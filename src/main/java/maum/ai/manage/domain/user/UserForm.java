package maum.ai.manage.domain.user;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonForm;

/**
 * 화면에서 사용하는 사용자 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class UserForm extends CommonForm {

	private Integer UserNo;
	private String Email;
	private String Password;
	private String Name;
	private String Phone;
	private String Active;
	private Integer PrivacyAgree;
	private String PrivacyDate;
	private Integer Enabled;
	private String Authority;
	private Integer LoginFailCNT;
	private String apiId;
	private String paymentDate;
	private String dateFrom;
	private String dateTo;
	private String status;

	private String search_type;
	private String search_keyword;
	private String search_category;
	private String search_status;
	private String search_name;
	private String search_email;
	private String search_company;
	private String search_product;
	private String search_paycount;
	private String search_paycountCondition;
	private String search_payment;
	private String search_apistatus;
	private String search_startPaymentDate;
	private String search_endPaymentDate;
	private String search_registerPath;
	private String search_marketingAgreeYes;
	private String search_marketingAgreeNo;

	private String search_nextPaymentDate;
	private String search_apiId;

	private Integer UserNoArr[];
}
