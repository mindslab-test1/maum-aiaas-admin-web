package maum.ai.manage.domain.user;

import lombok.Data;

@Data
public class UserCsvForm {
    private String num;
    private String name;
    private String email;
    private String phone;
    private String company;
    private String status;
    private String productName;
    private String payCount;
    private String payment;
    private String payDateFrom;
    private String payDate;
//    private String apiStatus;
//    private String usage;
    private String marketing;
    private String registerPath;
    private String createDate;
    private String updateDate;


    public void makeTableHead(){
        setNum("No.");
        setName("Name");
        setEmail("Email");
        setPhone("Phone");
        setCompany("Company");
        setStatus("Status");
        setProductName("Product");
        setPayCount("회차");
        setPayment("Payment");
        setPayDateFrom("Date From");
        setPayDate("Payment Date");
//        setApiStatus("API Status");
//        setUsage("Usage(%)");
        setMarketing("Marketing");
        setRegisterPath("Register Path");
        setCreateDate("Create Date");
        setUpdateDate("Update Date");
    }

}
