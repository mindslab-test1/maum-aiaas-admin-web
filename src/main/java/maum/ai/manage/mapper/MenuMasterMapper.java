package maum.ai.manage.mapper;

import maum.ai.manage.domain.admin.MenuDto;
import maum.ai.manage.domain.admin.MenuForm;
import maum.ai.manage.domain.admin.MenuMasterDto;
import maum.ai.manage.domain.admin.MenuMasterForm;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 매핑파일에 기재된 sql을 호출하기 위한 interface
 *
 * @author unongko
 * @version 1.0
 * @see /menuMaster.xml
 */

@Component
@Mapper
public interface MenuMasterMapper {

    /** 메인메뉴 - 목록 수 */
    int getMenuMasterCnt(MenuMasterForm menuMasterForm);
    /** 메인메뉴 - 목록 조회 */
    List<MenuMasterDto> getMenuMasterList(MenuMasterForm menuMasterForm);
    /** 메뉴마스터코드 - 목록 조회 */
    List<MenuMasterDto> getMenuMasterCodeList(MenuMasterForm menuMasterForm);
    /** 메인메뉴 상세 조회 */
    MenuMasterDto getMenuMasterDetail(Integer menuMasterNo);
    /** 메인메뉴 - 저장 */
    int mergeMenuMaster(MenuMasterForm menuMasterForm);
    /** 메인메뉴 - 단건삭제 */
    int deleteMenuMaster(MenuMasterForm menuMasterForm);

    /** 서브메뉴 - 목록 조회 */
    List<MenuDto> getMenuList(MenuForm menuForm);
    /** 서브메뉴코드 - 목록 조회 */
    List<MenuDto> getMenuCodeList(MenuForm menuForm);
    /** 서브메뉴 - 저장 */
    int mergeMenu(MenuForm menuForm);
    /** 서브메뉴 상세 조회 */
    MenuDto getMenuDetail(Integer menuNo);
    /** 서브메뉴 - 단건삭제 */
    int deleteMenu(MenuForm menuForm);

    /** LeftMenu - master 목록 조회 */
    List<MenuMasterDto> getLeftMenuMasterList(MenuMasterForm menuMasterForm);

    /** LeftMenu - sub 목록 조회 */
    List<MenuDto> getLeftMenuSubList(MenuForm menuForm);
}