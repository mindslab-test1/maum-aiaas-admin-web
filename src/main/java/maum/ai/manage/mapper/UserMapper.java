package maum.ai.manage.mapper;

import maum.ai.manage.domain.user.UserCsvForm;
import maum.ai.manage.domain.user.UserDto;
import maum.ai.manage.domain.user.UserForm;
import maum.ai.manage.entity.Pay;
import maum.ai.manage.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 매핑파일에 기재된 sql을 호출하기 위한 interface
 *
 * @author unongko
 * @version 1.0
 * @see /user.xml
 */

@Component
@Mapper
public interface UserMapper {

    /** 사용자 계정 - 목록 수 */
    int getUserCnt(UserForm userForm);
    /** 사용자 계정 - 목록 조회 */
    List<UserDto> getUserList(UserForm userForm);
    /** 사용자 계정 - CSV 목록 */
    List<UserCsvForm> getUserListForCsv(UserForm userForm);
    /** 사용자 상세 조회 */
    UserDto getUserDetail(String email);
    /** 사용자 - 정보 변경 */
    int updateUserInfo(User user);
    /** 사용자 id 중복 조회 */
    UserDto getUserId(String email);
    /** 사용자 apiId 중복 조회 */
    int getApiId(String apiId);
    /** 사용자 - 등록 */
    int insertUser(UserForm userForm);
    /** 사용자 - 단건삭제 */
    int deleteUser(UserForm userForm);
    /** 사용자 계정 - update status */
    int updateUserStatus(UserForm userForm);
    /** 사용자 계정 - update userServicePeriod*/
    int updateUserServicePeriod(UserForm userForm);
    /** 사용자 계정 - update Payment(결제방식) */
    int updatePayment(UserForm userForm);
    /** 사용자 계정 - update updateMarketingAgree */
    int updateMarketingAgree(UserForm userForm);
    /** 사용자 계정 - update nextPaymentDate (다음 결제일) */
    int updateNextPayment(UserForm userForm);
    /** 사용자 계정 - select billing_t createDate */
    String getCreateDate(UserForm userForm);
    /** 사용자 계정 - update createDate (다음 결제일) */
    int updateCreateDate(UserForm userForm);
    /** 사용자 계정 - update payCount (decrease) */
    int decreasePayCount(UserForm userForm);
    /** 사용자 계정 - billing info 조회 */
    Pay getUserBillingInfo(int USerNo);
    /** 사용자 계정 - delete billing info (사용자 빌링 정보 삭제) */
    int deleteBillingInfo(int UserNo);
    /** 사용자 계정 - insert billing info (사용자 빌링 정보 추가) */
    int insertBillingInfo(int UserNo);
    /** 사용자 계정 - update user Api Info (사용자 API 정보 변경) */
    int updateUserApiInfo(User user);
    /** 사용자 계정 - update agree marketingAgree */
    int updateAgreeMarketingAgree(int UserNo);
}