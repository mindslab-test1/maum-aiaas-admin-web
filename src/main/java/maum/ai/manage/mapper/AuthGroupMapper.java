package maum.ai.manage.mapper;

import maum.ai.manage.domain.admin.*;
import maum.ai.manage.entity.AuthGroup;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 매핑파일에 기재된 sql을 호출하기 위한 interface
 *
 * @author unongko
 * @version 1.0
 * @see /authGroup.xml
 */

@Component
@Mapper
public interface AuthGroupMapper {

    /** 권한그룹 - 목록 수 */
    int getAuthGroupCnt(AuthGroupForm authGroupForm);
    /** 권한그룹  - 목록 조회 */
    List<AuthGroupDto> getAuthGroupList(AuthGroupForm authGroupForm);
    /** 권한그룹 상세 조회 */
    AuthGroupDto getAuthGroupDetail(Integer authGroupNo);
    /** 권한그룹  - 정보 변경 */
    int updateAuthGroupInfo(AuthGroup authGroup);
    /** 권한그룹  - 저장 */
    int mergeAuthGroup(AuthGroupForm authGroupForm);
    /** 권한그룹  - 단건삭제 */
    int deleteAuthGroup(AuthGroupForm authGroupForm);
    /** 권한그룹 - selectBox 목록 조회 */
    List<AuthGroupDto> getAuthGroupSelectList(AuthGroupForm authGroupForm);

    /** 권한메뉴  - 저장 */
    int mergeAuthMenu(AuthMenuForm authMenuForm);
    /** 권한메뉴 - 목록 조회 */
    List<AuthMenuDto> getAuthMenuList(AuthMenuForm authMenuForm);
    /** 권한메뉴 - 단건삭제 */
    int deleteAuthMenu(AuthMenuForm authMenuForm);
    /** 권한메뉴  존재체크 */
    int getAuthMenuCheck(AuthMenuForm authMenuForm);
    /** 권한메뉴 상세 조회 */
    AuthMenuDto getAuthMenuDetail(Integer authMenuNo);

    /** 메뉴별 계정 권한 - 조회 */
    AuthMenuDto getUserAuthMenu(AuthMenuForm authMenuForm);
}