package maum.ai.manage.mapper;

import maum.ai.manage.domain.admin.AdminUserDto;
import maum.ai.manage.domain.admin.AdminUserForm;
import maum.ai.manage.domain.user.UserPrincipal;
import maum.ai.manage.entity.AdminUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 매핑파일에 기재된 sql을 호출하기 위한 interface
 *
 * @author unongko
 * @version 1.0
 * @see /adminUser.xml
 */

@Component
@Mapper
public interface AdminUserMapper {
    UserPrincipal findUserByLoginId(@Param("loginId") String loginId);

    /** 로그인 계정 상세 조회 */
    AdminUser getLoginAdminUserDetail(@Param("loginId") String userId);
    int updateAdminUserEnabled(AdminUser adminUser);
    int updateAdminUserFailCnt(AdminUser adminUser);

    /** 계정 프로필 - 비밀번호 변경 */
    int updateAdminUserPwd(AdminUser adminUser);

    /** 계정 - 목록 수 */
    int getAdminUserCnt(AdminUserForm adminUserForm);
    /** 계정 - 목록 조회 */
    List<AdminUserDto> getAdminUserList(AdminUserForm adminUserForm);
    /** 계정 상세 조회 */
    AdminUserDto getAdminUserDetail(Integer userNo);
    /** 계정 - 정보 변경 */
    int updateAdminUserInfo(AdminUser adminUser);
    /** 계정 id 중복 조회 */
    AdminUserDto getAdminUserId(String userId);
    /** 계정 - 등록 */
    int insertAdminUser(AdminUserForm adminUserForm);
    /** 계정 - 단건삭제 */
    int deleteAdminUser(AdminUserForm adminUserForm);
    /** 메뉴권한 사용자 - 목록 조회 */
    List<AdminUserDto> getMenuAuthAdminUserList(AdminUserForm adminUserForm);
}