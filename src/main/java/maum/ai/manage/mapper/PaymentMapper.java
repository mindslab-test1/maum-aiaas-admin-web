package maum.ai.manage.mapper;

import maum.ai.manage.domain.payment.PayDto;
import maum.ai.manage.domain.payment.PayForm;
import maum.ai.manage.domain.payment.PayCsvForm;
import maum.ai.manage.entity.Pay;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
@Mapper
public interface PaymentMapper {
    /**  목록 수 */
    int getPayCnt(PayForm payForm);
    /** 목록 조회 */
    List<PayDto> getPayList(PayForm payForm);
    List<PayCsvForm> getPayListForCsv(PayForm payForm);
    List<PayDto> todayPayList(String currentDate);
    List<Integer> numberOfPayment(PayForm payForm);
    int MAUCount(String currentDate);
    int MAUNullCheck(String currentDate);
    List<Integer> numberOfCustomer();
    List<PayDto> payFailList();
    /** 결제 취소시 */
    Pay getPayInfo(String tid);
    int updatePayTtoCancel(Pay pay);
    int insertLogPay(HashMap<String, Object> map);
}
