package maum.ai.manage.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonVo;

/**
 * 사용자 DB 모델 (User_t)
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class User extends CommonVo{
	private Integer UserNo;
	private String Name;
	private String ID;
	private String Password;
	private String Email;
	private String Phone;
	private Integer Grade;
	private Integer Product;
	private Integer Enabled;
	private String Authority;
	private Integer Type;
	private Integer Limit;
	private String Account;
	private Integer VoiceAgree;
	private Integer PrivacyAgree;
	private String PrivacyDate;
	private String LastLoginDate;
	private String LastLogoutDate;
	private Integer djangoid;
	private Integer Active;
	private String Lkey;
	private String Gkey;
	private Integer LoginFailCNT;
	private String NationCD;
	private String Company;
	private String Job;
	private Integer CreateFlag;
	private String CompanyEmail;
	private String ApiKey;
	private String ApiId;
	private Integer MarketingAgree;
	private String RegisterPath;
	private String MarketingDate;
	private Integer Status;
	private Integer PayCount;
}