package maum.ai.manage.entity;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonVo;

/**
 * 관리사용자 DB 모델 (AdminUser_t)
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AdminUser extends CommonVo{
	private Integer UserNo;
	private String UserId;
	private String UserNm;
	private String Password;
	private String Company;
	private String Dept;
	private String Phone;
	private String Email;
	private Integer Active;
	private Integer Enabled;
	private String Authority;
	private Integer LoginFailCNT;
	private Integer AuthGroupNo;
}