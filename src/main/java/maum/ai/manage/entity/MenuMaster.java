package maum.ai.manage.entity;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonVo;

/**
 * 메뉴마스터정보 DB 모델 (MenuMaster_t)
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class MenuMaster extends CommonVo{
	private Integer MenuMasterNo;
	private String MenuMasterType;
	private String MenuMasterNm;
	private String MenuMasterUrl;
	private String MenuMasterUseYN;
	private Integer MenuMasterOrder;
	private String LangType;
	private String IconNm;
}