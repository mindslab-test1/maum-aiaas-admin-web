package maum.ai.manage.entity;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonVo;

/**
 * 권한메뉴 DB 모델 (AuthMenu_t)
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AuthMenu extends CommonVo{
	private Integer AuthMenuNo;
	private Integer AuthGroupNo;
	private Integer MenuMasterNo;
	private Integer MenuNo;
	private String RoleCode;
}