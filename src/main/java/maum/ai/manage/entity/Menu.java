package maum.ai.manage.entity;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonVo;

/**
 * 메뉴 DB 모델 (Menu_t)
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class Menu extends CommonVo{
	private Integer MenuNo;
	private Integer MenuMasterNo;
	private String MenuNm;
	private String MenuUrl;
	private String MenuUseYN;
	private Integer MenuOrder;
}