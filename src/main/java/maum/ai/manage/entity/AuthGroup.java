package maum.ai.manage.entity;

import lombok.Getter;
import lombok.Setter;
import maum.ai.manage.domain.common.CommonVo;

/**
 * 권한그룹 DB 모델 (AuthMenu_t)
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AuthGroup extends CommonVo{
	private Integer AuthGroupNo;
	private String AuthGroupNm;
	private String Description;
}