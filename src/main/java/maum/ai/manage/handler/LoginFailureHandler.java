package maum.ai.manage.handler;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * 스프링시큐리티 로그인 실패 처리 핸들러
 *
 * @author unongko
 * @version 1.0
 */

@Component
public class LoginFailureHandler implements AuthenticationFailureHandler {
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException auth) throws IOException, ServletException {

		String errormsg = "errormsg";

		if(auth instanceof BadCredentialsException) {
			errormsg = "아이디나 비밀번호가 맞지 않습니다. 다시 확인해주세요.";
		} else if(auth instanceof LockedException) {
			errormsg = "계정이 잠겨있습니다. 관리자에게 문의하세요.";
		} else if(auth instanceof DisabledException) {
			errormsg = "계정이 잠겨있습니다. 관리자에게 문의하세요.";
		} else if(auth instanceof UsernameNotFoundException) {
			errormsg = "계정이 비활성 상태이거나 사용자가 존재하지 않습니다. 관리자에게 문의하세요.";
		}

		System.out.println("LoginFailureHandler errormsg ::: "+errormsg);

		response.sendRedirect(request.getContextPath() + "/login?error=true&errormsg="+ URLEncoder.encode(errormsg, "UTF-8"));
	}
}