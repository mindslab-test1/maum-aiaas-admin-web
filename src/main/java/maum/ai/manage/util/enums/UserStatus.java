package maum.ai.manage.util.enums;

import java.util.Arrays;

public enum UserStatus {
    INIT(0, "INIT", "UNREGISTERED"),
    FREE(1, "FREE", "ACTIVATE"),
    SUBSCRIBED(2, "SUBSCRIBED", "ACTIVATE"),
    UNSUBSCRIBING(3, "UNSUBSCRIBING", "ACTIVATE"),
    UNSUBSCRIBED(4, "UNSUBSCRIBED", "DEACTIVATE");

    int statusInt;
    String statusStr;
    String apiStatus;

    UserStatus(int statusInt, String statusStr, String apiStatus){
        this.statusInt = statusInt;
        this.statusStr = statusStr;
        this.apiStatus = apiStatus;
    }

    public int getStatusInt(){
        return this.statusInt;
    }
    public String getStatusStr(){
        return this.statusStr;
    }
    public String getApiStatus(){
        return this.apiStatus;
    }

    public static UserStatus getStatus(int numberOfMatch) {
        return Arrays.stream(values())
                .filter(userStatus -> userStatus.statusInt == numberOfMatch)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("일치하는 Status가 없습니다."));
    }
    
}
