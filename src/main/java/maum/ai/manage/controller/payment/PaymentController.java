package maum.ai.manage.controller.payment;


import com.google.gson.Gson;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.domain.payment.PayForm;
import maum.ai.manage.domain.user.UserForm;
import maum.ai.manage.entity.AdminUser;
import maum.ai.manage.service.pay.PaymentService;
import maum.ai.manage.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.spel.ast.NullLiteral;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 결제 컨트롤러
 *
 * @author miryoung
 * @version 1.0
 */

@Controller
public class PaymentController<PayFrom> {

    private static final Logger logger = LoggerFactory.getLogger(maum.ai.manage.controller.payment.PaymentController.class);

    @Autowired
    private PaymentService paymentService;

    @GetMapping("/payment/paymentList")
    public String userManagement(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("paymentList()");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "paymentHome");
        httpSession.setAttribute("selectSubMenu", "paymentList");
        httpSession.setAttribute("menuName", "결제내역");
        httpSession.setAttribute("subMenuName", "결제내역조회");

        AdminUser accessUser = (AdminUser) httpSession.getAttribute("accessUser");
        model.addAttribute("userName",accessUser.getUserNm());

        return "payment/paymentList";
    }



    /** 결제 - 결제 내역 조회 */
    @RequestMapping(value = "/payment/getPayList")
    @ResponseBody
    public ResultDto getPayList(HttpServletRequest request, HttpServletResponse response, PayForm payForm) throws Exception {
        logger.info("getPayList()@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        Gson gson = new Gson();

        logger.debug("Name :: {} {}", payForm.getName(), payForm.getName().getClass());

        logger.debug("{} .getPayList()", gson.toJson(payForm));
        ResultDto resultDto = paymentService.getPayList(payForm);
        logger.debug("getPayList() resultDto ======= " + gson.toJson(resultDto));

        return resultDto;
    }


    /** 결제 - 취소 */
    @RequestMapping(value="/payment/payCancel", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto payCancel(HttpServletRequest request, @RequestParam(value="cancelTid")String cancelTid, @RequestParam(value="cancelPayment")String cancelPayment){
        logger.debug("{} :: payCancel() controller", this.getClass().getSimpleName());
        ResultDto resultDto = new ResultDto();
        int sessionUser = -1;

        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            AdminUser accessUser = (AdminUser) httpSession.getAttribute("accessUser");
            if(accessUser != null) {
                sessionUser = accessUser.getUserNo();
            }
        }

        logger.info("Tid = {}, payment = {}", cancelTid, cancelPayment);

        try {
            resultDto = paymentService.payCancel(request, sessionUser, cancelTid, cancelPayment);
        } catch (Exception e) {
            e.printStackTrace();
            return resultDto;
        }

        return resultDto;
    }


}