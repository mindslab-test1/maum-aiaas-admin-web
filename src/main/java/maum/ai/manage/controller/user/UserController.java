package maum.ai.manage.controller.user;

import com.google.gson.Gson;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.domain.user.UserDto;
import maum.ai.manage.domain.user.UserForm;
import maum.ai.manage.entity.AdminUser;
import maum.ai.manage.entity.User;
import maum.ai.manage.mapper.UserMapper;
import maum.ai.manage.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * 사용자 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class UserController<UserFrom> {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @GetMapping("/user/userManagement")
    public String userManagement(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("userManagement()");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "userHome");
        httpSession.setAttribute("selectSubMenu", "userList");
        httpSession.setAttribute("menuName", "회원관리");
        httpSession.setAttribute("subMenuName", "회원조회");


        String category = request.getParameter("category");
        System.out.println("this is category : "+category);

        model.addAttribute("category", category);

        return "user/userManagement";
    }

    @GetMapping("/user/userDetail")
    public String userDetail(@RequestParam("email") String email, HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("userDetail() ==> " + email);
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "userHome");
        httpSession.setAttribute("menuName", "회원관리");
        httpSession.setAttribute("subMenuName", "회원조회");

        UserDto userDto = new UserDto();
        Gson gson = new Gson();
        AdminUser accessUser = (AdminUser) httpSession.getAttribute("accessUser");
        try {

            userDto = userService.getUserDetail(email);
            logger.info(" @ userDetail() Gson userDto" + gson.toJson(userDto));

            logger.info(" @ userDetail() userDto" + userDto.toString());
        } catch (Exception e) {
            logger.info("userDetail() Error : " + e);
        }

        model.addAttribute("userInfo", userDto);
        model.addAttribute("userName",accessUser.getUserNm());
        return "user/userDetail";
    }


    /** 사용자 - 목록 조회 */
    @RequestMapping(value = "/user/getUserList")
    @ResponseBody
    public ResultDto getUserList(HttpServletRequest request, HttpServletResponse response, UserForm userForm) throws Exception {
        logger.info("getUserList()");
        Gson gson = new Gson();

        logger.info("{} .getUserList()", gson.toJson(userForm));
        ResultDto resultDto = userService.getUserList(userForm);
//        System.out.println("getUserList() resultDto ======= " + gson.toJson(resultDto));

        return resultDto;
    }

    /** 사용자 상세 조회 */
    @RequestMapping(value = "/user/getUserDetail")
    @ResponseBody
    public UserDto getUserDetail(HttpServletRequest request, HttpServletResponse response, UserForm userForm) throws Exception {

        UserDto userDto = new UserDto();

        Gson gson = new Gson();

        logger.info("{} .getUserDetail() userForm", gson.toJson(userForm));

        userDto = userService.getUserDetail(userForm.getEmail());

        logger.info("{} .getUserDetail() userDto", gson.toJson(userDto));

        return userDto;
    }

    /** 사용자 - 정보 수정 */
    @RequestMapping(value = "/user/updateUserInfo")
    @ResponseBody
    public UserDto updateUserInfo(HttpServletRequest request, HttpServletResponse response, User user) throws Exception {

        UserDto userDto = new UserDto();

        Gson gson = new Gson();

        logger.info("{} .updateUserInfo()", gson.toJson(user));

        userDto = userService.updateUserInfo(user);

        return userDto;
    }

    /** 사용자 id 조회 */
    @RequestMapping(value = "/user/getUserId")
    @ResponseBody
    public UserDto getUserId(HttpServletRequest request, HttpServletResponse response, User user) throws Exception {

        UserDto userDto = new UserDto();

        Gson gson = new Gson();

        logger.info("{} .getUserId()", gson.toJson(user));

        userDto = userService.getUserId(user.getEmail());

        return userDto;
    }

    /** 사용자 - 등록 */
    @RequestMapping(value = "/user/insertUser")
    @ResponseBody
    public UserDto insertUser(HttpServletRequest request, HttpServletResponse response, UserForm userForm) throws Exception {

        Gson gson = new Gson();

        logger.info("{} .insertUser()", gson.toJson(userForm));
        System.out.println("insertUser() ======= " + gson.toJson(userForm));

        UserDto userDto = userService.insertUser(userForm);

        return userDto;
    }

    /** 사용자 - 다건 삭제 */
    @RequestMapping(value = "/user/deleteMultiUser")
    @ResponseBody
    public UserDto deleteMultiUser(HttpServletRequest request, HttpServletResponse response, UserForm userForm) throws Exception {

        UserDto userDto = new UserDto();
        Gson gson = new Gson();
        logger.info("{} .deleteMultiUser()", gson.toJson(userForm));
        userDto = userService.deleteMultiUser(userForm);
        return userDto;
    }

    /** 계정 프로필 - status update */
    @RequestMapping(value = "/user/updateUserStatus")
    @ResponseBody
    public ResponseEntity<Map<String, String>> updateUserStatus(UserForm userForm){

        logger.info("updateUserStatus() : {}", new Gson().toJson(userForm));
        return userService.updateUserStatus(userForm);

    }

    /** 계정 프로필 - 서비스 기간 update */
    @RequestMapping(value = "/user/updateUserServicePeriod")
    @ResponseBody
    public int updateUserServicePeriod(UserForm userForm, HttpServletRequest request, HttpServletResponse response){

        UserDto userDto = new UserDto();
        Gson gson = new Gson();

        logger.info("updateUserServicePeriod() : {}", gson.toJson(userForm));

        //System.out.println(" @ @ @ ************** " + userForm.getUserNo());

        int result = userService.updateUserServicePeriod(userForm);
        return result;
    }

    /** 계정 프로필 - payment(결제방식) update */
    @RequestMapping(value="/user/updatePayment")
    @ResponseBody
    public int updatePayment(UserForm userForm, HttpServletRequest request, HttpServletResponse response) {

        UserDto userDto = new UserDto();
        Gson gson = new Gson();

        logger.info("updatePayment() : {}", gson.toJson(userForm));

        int result = userService.updatePayment(userForm);
        return result;
    }

    /** 계정 프로필 - MarketingAgree update */
    @RequestMapping(value="/user/updateMarketingAgree")
    @ResponseBody
    public int updateMarketingAgree(UserForm userForm, HttpServletRequest request, HttpServletResponse response) {

        UserDto userDto = new UserDto();
        Gson gson = new Gson();

        logger.info("updateMarketingAgree() : {}", gson.toJson(userForm));

        int result = userService.updateMarketingAgree(userForm);
        return result;
    }

    /** 계정 프로필 - nextPayment(다음 결제일) update */
    @RequestMapping(value = "user/updateNextPayment")
    @ResponseBody
    public int updateNextPayment(UserForm userForm, HttpServletRequest request, HttpServletResponse response) {

        UserDto userDto = new UserDto();
        Gson gson = new Gson();

        logger.info("updateNextPayment() : {}", gson.toJson(userForm));

        int result = userService.updateNextPayment(userForm);
        return result;
    }

    /** 계정 프로필 - decrease payCount update */
    @RequestMapping(value = "/user/decreasePayCount")
    @ResponseBody
    public int decreasePayCount(UserForm userForm, HttpServletRequest request, HttpServletResponse response) {

        UserDto userDto = new UserDto();
        Gson gson = new Gson();

        logger.info("decreasePayCount() : {}", gson.toJson(userForm));

        int result = userService.decreasePayCount(userForm);
        return result;
    }

    /** 계정 프로필 - 사용자 결제 방식 Cash로 변경 */
    @RequestMapping(value = "/user/changePaymentCash")
    @ResponseBody
    public int changePaymentCash(UserForm userForm, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Gson gson = new Gson();

        logger.info("changePaymentCash() : {}", gson.toJson(userForm));

        int result = userService.changePaymentCash(userForm);
        return result;
    }
}