package maum.ai.manage.controller.test;

import maum.ai.manage.config.PropertyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class TestRestController {

    @Autowired
    PropertyConfig propertyConfig;

    @RequestMapping("/test")
    public String test() {

        String prof =  System.getProperty("spring.profiles.active");

        String returnStr = "Server : " + prof;

        returnStr = returnStr + " : "+ propertyConfig.getObj().getNumber() + " / " + propertyConfig.getObj().getText();

        return returnStr;
    }
}
