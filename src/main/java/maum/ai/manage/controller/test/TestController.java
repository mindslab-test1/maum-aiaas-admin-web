package maum.ai.manage.controller.test;

import maum.ai.manage.config.PropertyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class TestController {

    @GetMapping("/index")
    public String home(HttpServletRequest request, HttpServletResponse response, Model model) {

        return "index";
    }
}
