package maum.ai.manage.controller.admin;

import com.google.gson.Gson;
import maum.ai.manage.domain.admin.*;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.entity.AdminUser;
import maum.ai.manage.service.admin.AuthGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 권한그룹 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class AuthGroupController {

    private static final Logger logger = LoggerFactory.getLogger(AuthGroupController.class);

    @Autowired
    private AuthGroupService authGroupService;

    @GetMapping("/admin/authGroupList")
    public String authGroupList(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("authGroupList()");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "설정");
        httpSession.setAttribute("subMenuName", "권한그룹관리");

        return "admin/authGroupList";
    }

    /** 권한그룹 - 목록 조회 */
    @RequestMapping(value = "/admin/getAuthGroupList")
    @ResponseBody
    public ResultDto getAuthGroupList(HttpServletRequest request, HttpServletResponse response, AuthGroupForm authGroupForm) throws Exception {
        logger.info("getAuthGroupList()");
        Gson gson = new Gson();

        logger.info("getAuthGroupList()  authGroupForm : {} ", gson.toJson(authGroupForm));
        ResultDto resultDto = authGroupService.getAuthGroupList(authGroupForm);
        System.out.println("getAuthGroupList() resultDto ======= " + gson.toJson(resultDto));

        return resultDto;
    }

    /** 권한그룹 상세 조회 */
    @RequestMapping(value = "/admin/getAuthGroupDetail")
    @ResponseBody
    public AuthGroupDto getAuthGroupDetail(HttpServletRequest request, HttpServletResponse response, AuthGroupForm authGroupForm) throws Exception {

        AuthGroupDto authGroupDto = new AuthGroupDto();

        Gson gson = new Gson();

        logger.info("getAuthGroupDetail() authGroupForm  : {}", gson.toJson(authGroupForm));

        authGroupDto = authGroupService.getAuthGroupDetail(authGroupForm.getAuthGroupNo());

        logger.info("getAuthGroupDetail() authGroupDto : {}", gson.toJson(authGroupDto));

        return authGroupDto;
    }

    /** 권한그룹 - 저장 */

    @RequestMapping(value = "/admin/mergeAuthGroup")
    @ResponseBody
    public AuthGroupDto mergeAuthGroup(HttpServletRequest request, HttpServletResponse response, AuthGroupForm authGroupForm) throws Exception {

        Gson gson = new Gson();

        logger.info("mergeAuthGroup() : {}", gson.toJson(authGroupForm));
        System.out.println("mergeAuthGroup() : {}" + gson.toJson(authGroupForm));

        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            AdminUser adminUser = (AdminUser) httpSession.getAttribute("accessUser");
            if(adminUser != null) {
                int userNo = adminUser.getUserNo();
                if(authGroupForm.getAuthGroupNo() == null){
                    authGroupForm.setAuthGroupNo(0);
                    authGroupForm.setCreateUser(userNo);
                }else{
                    authGroupForm.setUpdateUser(userNo);
                }
            }
        }

        AuthGroupDto authGroupDto = authGroupService.mergeAuthGroup(authGroupForm);

        return authGroupDto;
    }


    /** 권한그룹 - 다건 삭제 */
    @RequestMapping(value = "/admin/deleteMultiAuthGroup")
    @ResponseBody
    public AuthGroupDto deleteMultiAuthGroup(HttpServletRequest request, HttpServletResponse response, AuthGroupForm authGroupForm) throws Exception {

        AuthGroupDto authGroupDto = new AuthGroupDto();
        Gson gson = new Gson();
        logger.info("deleteMultiAuthGroup() : {}", gson.toJson(authGroupForm));
        authGroupDto = authGroupService.deleteMultiAuthGroup(authGroupForm);
        return authGroupDto;
    }

    /** 권한메뉴 - 저장 */
    @RequestMapping(value = "/admin/mergeAuthMenu")
    @ResponseBody
    public AuthMenuDto mergeAuthMenu(HttpServletRequest request, HttpServletResponse response, AuthMenuForm authMenuForm) throws Exception {

        Gson gson = new Gson();

        logger.info("mergeAuthMenu() : {}", gson.toJson(authMenuForm));

        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            AdminUser adminUser = (AdminUser) httpSession.getAttribute("accessUser");
            if(adminUser != null) {
                int userNo = adminUser.getUserNo();
                if(authMenuForm.getAuthMenuNo() == null){
                    authMenuForm.setAuthMenuNo(0);
                    authMenuForm.setCreateUser(userNo);
                }else{
                    authMenuForm.setUpdateUser(userNo);
                }
            }
        }

        AuthMenuDto authMenuDto = authGroupService.mergeAuthMenu(authMenuForm);

        return authMenuDto;
    }

    /** 권한메뉴 - 목록 조회 */
    @RequestMapping(value = "/admin/getAuthMenuList")
    @ResponseBody
    public ResultDto getAuthMenuList(HttpServletRequest request, HttpServletResponse response, AuthMenuForm authMenuForm) throws Exception {
        logger.info("getAuthMenuList()");
        Gson gson = new Gson();

        logger.info("getAuthMenuList() : {} ", gson.toJson(authMenuForm));
        ResultDto resultDto = authGroupService.getAuthMenuList(authMenuForm);
        logger.info("getAuthMenuList() resultDto : {} ", gson.toJson(resultDto));

        return resultDto;
    }

    /** 권한메뉴 - 다건 삭제 */
    @RequestMapping(value = "/admin/deleteMultiAuthMenu")
    @ResponseBody
    public AuthMenuDto deleteMultiAuthMenu(HttpServletRequest request, HttpServletResponse response, AuthMenuForm authMenuForm) throws Exception {

        AuthMenuDto authMenuDto = new AuthMenuDto();
        Gson gson = new Gson();
        logger.info("deleteMultiAuthMenu() : {}", gson.toJson(authMenuForm));
        authMenuDto = authGroupService.deleteMultiAuthMenu(authMenuForm);
        return authMenuDto;
    }

    /** 권한메뉴 상세 조회 */
    @RequestMapping(value = "/admin/getAuthMenuDetail")
    @ResponseBody
    public AuthMenuDto getAuthMenuDetail(HttpServletRequest request, HttpServletResponse response, AuthMenuForm authMenuForm) throws Exception {

        AuthMenuDto authMenuDto = new AuthMenuDto();

        Gson gson = new Gson();

        logger.info("getAuthMenuDetail() authMenuForm  : {}", gson.toJson(authMenuForm));
        authMenuDto = authGroupService.getAuthMenuDetail(authMenuForm.getAuthMenuNo());
        logger.info("getAuthMenuDetail() authMenuDto : {}", gson.toJson(authMenuDto));

        return authMenuDto;
    }

    /** 권한그룹 - selectBox 목록 조회 */
    @RequestMapping(value = "/admin/getAuthGroupSelectList")
    @ResponseBody
    public ResultDto getAuthGroupSelectList(HttpServletRequest request, HttpServletResponse response, AuthGroupForm authGroupForm) throws Exception {
        logger.info("getAuthGroupSelectList()");
        Gson gson = new Gson();

        logger.info("getAuthGroupSelectList()  authGroupForm : {} ", gson.toJson(authGroupForm));
        ResultDto resultDto = authGroupService.getAuthGroupSelectList(authGroupForm);
        System.out.println("getAuthGroupSelectList() resultDto ======= " + gson.toJson(resultDto));

        if(authGroupForm.getAuthGroupNo() != null){
            resultDto.setSelectedInt(authGroupForm.getAuthGroupNo());
        }else{
            resultDto.setSelectedInt(0);
        }
        resultDto.setViewType(authGroupForm.getViewType());

        return resultDto;
    }
}