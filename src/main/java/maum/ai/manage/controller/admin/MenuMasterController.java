package maum.ai.manage.controller.admin;

import com.google.gson.Gson;
import maum.ai.manage.domain.admin.*;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.entity.AdminUser;
import maum.ai.manage.service.admin.MenuMasterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 메뉴 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class MenuMasterController {

    private static final Logger logger = LoggerFactory.getLogger(MenuMasterController.class);

    @Autowired
    private MenuMasterService menuMasterService;
    
    @GetMapping("/admin/menuMasterList")
    public String menuMasterList(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("menuMasterList()");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "adminHome");
        httpSession.setAttribute("selectSubMenu", "menuMasterList");
        httpSession.setAttribute("menuName", "설정");
        httpSession.setAttribute("subMenuName", "메뉴관리");

        return "admin/menuMasterList";
    }

    /** 메인메뉴 - 목록 조회 */
    @RequestMapping(value = "/admin/getMenuMasterList")
    @ResponseBody
    public ResultDto getMenuMasterList(HttpServletRequest request, HttpServletResponse response, MenuMasterForm menuMasterForm) throws Exception {
        logger.info("getMenuMasterList()");
        Gson gson = new Gson();

        logger.info("getMenuMasterList() : {} ", gson.toJson(menuMasterForm));
        ResultDto resultDto = menuMasterService.getMenuMasterList(menuMasterForm);
        logger.info("getMenuMasterList() resultDto : {} ", gson.toJson(resultDto));

        return resultDto;
    }

    /** 메인메뉴 상세 조회 */
    @RequestMapping(value = "/admin/getMenuMasterDetail")
    @ResponseBody
    public MenuMasterDto getMenuMasterDetail(HttpServletRequest request, HttpServletResponse response, MenuMasterForm menuMasterForm) throws Exception {

        MenuMasterDto menuMasterDto = new MenuMasterDto();

        Gson gson = new Gson();

        logger.info("getMenuMasterDetail() menuMasterForm  : {}", gson.toJson(menuMasterForm));

        menuMasterDto = menuMasterService.getMenuMasterDetail(menuMasterForm.getMenuMasterNo());

        logger.info("getMenuMasterDetail() menuMasterDto : {}", gson.toJson(menuMasterDto));

        return menuMasterDto;
    }

    /** 메인메뉴 - 저장 */
    @RequestMapping(value = "/admin/mergeMenuMaster")
    @ResponseBody
    public MenuMasterDto mergeMenuMaster(HttpServletRequest request, HttpServletResponse response, MenuMasterForm menuMasterForm) throws Exception {

        Gson gson = new Gson();

        logger.info("mergeMenuMaster() : {}", gson.toJson(menuMasterForm));

        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            AdminUser adminUser = (AdminUser) httpSession.getAttribute("accessUser");
            if(adminUser != null) {
                int userNo = adminUser.getUserNo();
                if(menuMasterForm.getMenuMasterNo() == null){
                    menuMasterForm.setMenuMasterNo(0);
                    menuMasterForm.setCreateUser(userNo);
                }else{
                    menuMasterForm.setUpdateUser(userNo);
                }
            }
        }

        MenuMasterDto menuMasterDto = menuMasterService.mergeMenuMaster(menuMasterForm);

        return menuMasterDto;
    }


    /** 메인메뉴 - 다건 삭제 */
    @RequestMapping(value = "/admin/deleteMultiMenuMaster")
    @ResponseBody
    public MenuMasterDto deleteMultiMenuMaster(HttpServletRequest request, HttpServletResponse response, MenuMasterForm menuMasterForm) throws Exception {

        MenuMasterDto menuMasterDto = new MenuMasterDto();
        Gson gson = new Gson();
        logger.info("deleteMultiMenuMaster() : {}", gson.toJson(menuMasterForm));
        menuMasterDto = menuMasterService.deleteMultiMenuMaster(menuMasterForm);
        return menuMasterDto;
    }

    /** 서브메뉴 - 목록 조회 */
    @RequestMapping(value = "/admin/getMenuList")
    @ResponseBody
    public ResultDto getMenuList(HttpServletRequest request, HttpServletResponse response, MenuForm menuForm) throws Exception {
        logger.info("getMenuList()");
        Gson gson = new Gson();

        logger.info("getMenuList() : {} ", gson.toJson(menuForm));
        ResultDto resultDto = menuMasterService.getMenuList(menuForm);
        logger.info("getMenuList() resultDto : {} ", gson.toJson(resultDto));

        return resultDto;
    }

    /** 서브메뉴 - 저장 */

    @RequestMapping(value = "/admin/mergeMenu")
    @ResponseBody
    public MenuDto mergeMenu(HttpServletRequest request, HttpServletResponse response, MenuForm menuForm) throws Exception {

        Gson gson = new Gson();

        logger.info("mergeMenu() : {}", gson.toJson(menuForm));

        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            AdminUser adminUser = (AdminUser) httpSession.getAttribute("accessUser");
            if(adminUser != null) {
                int userNo = adminUser.getUserNo();
                if(menuForm.getMenuNo() == null){
                    menuForm.setMenuNo(0);
                    menuForm.setCreateUser(userNo);
                }else{
                    menuForm.setUpdateUser(userNo);
                }
            }
        }

        MenuDto menuDto = menuMasterService.mergeMenu(menuForm);

        return menuDto;
    }

    /** 서브메뉴 상세 조회 */
    @RequestMapping(value = "/admin/getMenuDetail")
    @ResponseBody
    public MenuDto getMenuDetail(HttpServletRequest request, HttpServletResponse response, MenuForm menuForm) throws Exception {

        MenuDto menuDto = new MenuDto();

        Gson gson = new Gson();

        logger.info("getMenuDetail() menuForm  : {}", gson.toJson(menuForm));
        menuDto = menuMasterService.getMenuDetail(menuForm.getMenuNo());
        logger.info("getMenuDetail() menuDto : {}", gson.toJson(menuDto));

        return menuDto;
    }

    /** 서브메뉴 - 다건 삭제 */
    @RequestMapping(value = "/admin/deleteMultiMenu")
    @ResponseBody
    public MenuDto deleteMultiMenu(HttpServletRequest request, HttpServletResponse response, MenuForm menuForm) throws Exception {

        MenuDto menuDto = new MenuDto();
        Gson gson = new Gson();
        logger.info("deleteMultiMenu() : {}", gson.toJson(menuForm));
        menuDto = menuMasterService.deleteMultiMenu(menuForm);
        return menuDto;
    }

    /** 메뉴마스터코드 - 목록 조회 */
    @RequestMapping(value = "/admin/getMenuMasterCodeList")
    @ResponseBody
    public ResultDto getMenuMasterCodeList(HttpServletRequest request, HttpServletResponse response, MenuMasterForm menuMasterForm) throws Exception {
        logger.info("getMenuMasterCodeList()");
        Gson gson = new Gson();

        logger.info("getMenuMasterCodeList() : {} ", gson.toJson(menuMasterForm));
        ResultDto resultDto = menuMasterService.getMenuMasterCodeList(menuMasterForm);
        logger.info("getMenuMasterCodeList() resultDto : {} ", gson.toJson(resultDto));

        if(menuMasterForm.getMenuMasterNo() != null){
            resultDto.setSelectedInt(menuMasterForm.getMenuMasterNo());
        }else{
            resultDto.setSelectedInt(0);
        }
        resultDto.setViewType(menuMasterForm.getViewType());

        return resultDto;
    }

    /** 서브메뉴코드 - 목록 조회 */
    @RequestMapping(value = "/admin/getMenuCodeList")
    @ResponseBody
    public ResultDto getMenuCodeList(HttpServletRequest request, HttpServletResponse response, MenuForm menuForm) throws Exception {
        logger.info("getMenuCodeList()");
        Gson gson = new Gson();

        logger.info("getMenuCodeList() : {} ", gson.toJson(menuForm));
        ResultDto resultDto = menuMasterService.getMenuCodeList(menuForm);
        logger.info("getMenuCodeList() resultDto : {} ", gson.toJson(resultDto));

        if(menuForm.getMenuNo() != null){
            resultDto.setSelectedInt(menuForm.getMenuNo());
        }else{
            resultDto.setSelectedInt(0);
        }
        resultDto.setViewType(menuForm.getViewType());

        return resultDto;
    }
}