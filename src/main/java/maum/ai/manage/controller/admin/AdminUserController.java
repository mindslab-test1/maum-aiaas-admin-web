package maum.ai.manage.controller.admin;

import com.google.gson.Gson;
import maum.ai.manage.domain.admin.AdminUserDto;
import maum.ai.manage.domain.admin.AdminUserForm;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.entity.AdminUser;
import maum.ai.manage.service.admin.AdminUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 사용자 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class AdminUserController {

    private static final Logger logger = LoggerFactory.getLogger(AdminUserController.class);

    @Autowired
    private AdminUserService adminUserService;

    /** 계정 - 비밀번호 수정 */
    @RequestMapping(value = "/admin/updateAdminUserPwd")
    @ResponseBody
    public AdminUserDto updateAdminUserPwd(HttpServletRequest request, HttpServletResponse response, AdminUser adminUser) throws Exception {

        AdminUserDto adminUserDto = new AdminUserDto();

        Gson gson = new Gson();

        logger.info("updateAdminUserPwd() : {}", gson.toJson(adminUser));
        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            AdminUser accessUser = (AdminUser) httpSession.getAttribute("accessUser");
            if(accessUser != null) {
                adminUser.setUpdateUser(accessUser.getUserNo());
            }
        }
        adminUserDto = adminUserService.updateAdminUserPwd(adminUser);

        return adminUserDto;
    }

    @GetMapping("/admin/adminUserList")
    public String adminUserList(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("adminUserList()");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "adminHome");
        httpSession.setAttribute("selectSubMenu", "adminList");
        httpSession.setAttribute("menuName", "설정");
        httpSession.setAttribute("subMenuName", "계정관리");

        return "admin/adminUserList";
    }

    /** 계정 - 목록 조회 */

    @RequestMapping(value = "/admin/getAdminUserList")
    @ResponseBody
    public ResultDto getUserList(HttpServletRequest request, HttpServletResponse response, AdminUserForm adminUserForm) throws Exception {
        logger.info("getAdminUserList()");
        Gson gson = new Gson();

        logger.info("getAdminUserList()  adminUserForm : {} ", gson.toJson(adminUserForm));
        ResultDto resultDto = adminUserService.getAdminUserList(adminUserForm);
        logger.info("getAdminUserList() resultDto : {} " , gson.toJson(resultDto));

        return resultDto;
    }


    /** 계정 상세 조회 */

    @RequestMapping(value = "/admin/getAdminUserDetail")
    @ResponseBody
    public AdminUserDto getAdminUserDetail(HttpServletRequest request, HttpServletResponse response, AdminUserForm adminUserForm) throws Exception {

        AdminUserDto adminUserDto = new AdminUserDto();

        Gson gson = new Gson();

        logger.info("getAdminUserDetail() adminUserForm  : {}", gson.toJson(adminUserForm));
        adminUserDto = adminUserService.getAdminUserDetail(adminUserForm.getUserNo());
        logger.info("getAdminUserDetail() adminUserDto : {}", gson.toJson(adminUserDto));

        return adminUserDto;
    }


    /** 계정 - 정보 수정 */

    @RequestMapping(value = "/admin/updateAdminUserInfo")
    @ResponseBody
    public AdminUserDto updateAdminUserInfo(HttpServletRequest request, HttpServletResponse response, AdminUser adminUser) throws Exception {

        AdminUserDto adminUserDto = new AdminUserDto();

        Gson gson = new Gson();

        logger.info("updateAdminUserInfo() : {}", gson.toJson(adminUser));

        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            AdminUser accessUser = (AdminUser) httpSession.getAttribute("accessUser");
            if(accessUser != null) {
                adminUser.setUpdateUser(accessUser.getUserNo());
            }
        }

        adminUserDto = adminUserService.updateAdminUserInfo(adminUser);

        return adminUserDto;
    }

    /** 계정 id 조회 */

    @RequestMapping(value = "/admin/getAdminUserId")
    @ResponseBody
    public AdminUserDto getAdminUserId(HttpServletRequest request, HttpServletResponse response, AdminUser adminUser) throws Exception {

        AdminUserDto adminUserDto = new AdminUserDto();

        Gson gson = new Gson();

        logger.info("getAdminUserId() : {}", gson.toJson(adminUser));

        adminUserDto = adminUserService.getAdminUserId(adminUser.getUserId());

        return adminUserDto;
    }


    /** 계정 - 등록 */

    @RequestMapping(value = "/admin/insertAdminUser")
    @ResponseBody
    public AdminUserDto insertAdminUser(HttpServletRequest request, HttpServletResponse response, AdminUserForm adminUserForm) throws Exception {

        Gson gson = new Gson();

        logger.info("insertAdminUser() : {}", gson.toJson(adminUserForm));
        System.out.println("insertAdminUser() : {}" + gson.toJson(adminUserForm));
        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            AdminUser accessUser = (AdminUser) httpSession.getAttribute("accessUser");
            if(accessUser != null) {
                adminUserForm.setCreateUser(accessUser.getUserNo());
            }
        }
        AdminUserDto adminUserDto = adminUserService.insertAdminUser(adminUserForm);

        return adminUserDto;
    }


    /** 계정 - 다건 삭제 */

    @RequestMapping(value = "/admin/deleteMultiAdminUser")
    @ResponseBody
    public AdminUserDto deleteMultiAdminUser(HttpServletRequest request, HttpServletResponse response, AdminUserForm adminUserForm) throws Exception {

        AdminUserDto adminUserDto = new AdminUserDto();
        Gson gson = new Gson();
        logger.info("deleteMultiAdminUser() : {}", gson.toJson(adminUserForm));
        adminUserDto = adminUserService.deleteMultiAdminUser(adminUserForm);
        return adminUserDto;
    }

    /** 메뉴권한 사용자 - 목록 조회 */

    @RequestMapping(value = "/admin/getMenuAuthAdminUserList")
    @ResponseBody
    public ResultDto getMenuAuthAdminUserList(HttpServletRequest request, HttpServletResponse response, AdminUserForm adminUserForm) throws Exception {
        logger.info("getMenuAuthAdminUserList()");
        Gson gson = new Gson();

        logger.info("getMenuAuthAdminUserList()  adminUserForm : {} ", gson.toJson(adminUserForm));
        ResultDto resultDto = adminUserService.getMenuAuthAdminUserList(adminUserForm);

        if(adminUserForm.getUserNo() != null){
            resultDto.setSelectedInt(adminUserForm.getUserNo());
        }else{
            resultDto.setSelectedInt(0);
        }
        resultDto.setViewType(adminUserForm.getViewType());

        logger.info("getMenuAuthAdminUserList() resultDto ::: {} ", gson.toJson(resultDto));

        return resultDto;
    }

}