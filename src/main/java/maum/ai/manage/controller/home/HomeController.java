package maum.ai.manage.controller.home;

import maum.ai.manage.domain.payment.PayDto;
import maum.ai.manage.entity.Pay;
import maum.ai.manage.mapper.PaymentMapper;
import maum.ai.manage.service.pay.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * 메인 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private PaymentMapper paymentMapper;
    @Autowired
    private PaymentService paymentService;

    @GetMapping("/home")
    public ModelAndView home(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("HomeController.{}", "home()");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "dashboardHome");
        httpSession.setAttribute("menuName", "DASHBOARD");
        httpSession.setAttribute("subMenuName", "");

        ModelAndView modelAndView = new ModelAndView();

        List<Integer> list = paymentService.getNumberOfCustomer();

        logger.info("this is customer list : {}", list);

        modelAndView.addObject("business", list.get(0));
        modelAndView.addObject("innerCust", list.get(1));
        modelAndView.addObject("outterCust", list.get(0)-(list.get(1) + list.get(2)));
        modelAndView.addObject("etc", list.get(2));
        modelAndView.setViewName("home/dashboard");

        return modelAndView;
    }

    @RequestMapping(value = "/pay/todayPayList")
    @ResponseBody
    public List<PayDto> todayPayList(){

        return paymentService.getTodayPayList();
    }

    @RequestMapping(value = "/pay/MAUCount")
    @ResponseBody
    public Map<String, Object> MAUCount(){
        final String DATE_PATTERN = "yyyy-MM-dd";
        Map<String, Object> resultMap = new HashMap<>();

        resultMap = paymentService.getMAUCount();

        return resultMap;
    }

    @RequestMapping(value="/pay/sales")
    @ResponseBody
    public List<Integer> sales(){

        return paymentService.getSales();
    }

    @RequestMapping(value="/pay/searchUser")
    public String searchUser(@RequestParam String category){

        return "/user/userManagement";
    }
}