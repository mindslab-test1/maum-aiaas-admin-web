package maum.ai.manage.controller.common;

import com.google.gson.Gson;
import maum.ai.manage.domain.admin.AuthMenuDto;
import maum.ai.manage.domain.admin.AuthMenuForm;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.domain.admin.MenuMasterForm;
import maum.ai.manage.entity.AdminUser;
import maum.ai.manage.service.admin.AuthGroupService;
import maum.ai.manage.service.admin.MenuMasterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 공통 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class CommonController {

    private static final Logger logger = LoggerFactory.getLogger(CommonController.class);

    @Autowired
    private MenuMasterService menuMasterService;

    @Autowired
    private AuthGroupService authGroupService;

    /** LeftMenu - 목록 조회 */
    @RequestMapping(value = "/common/getLeftMenuList")
    @ResponseBody
    public ResultDto getLeftMenuList(HttpServletRequest request, HttpServletResponse response, MenuMasterForm menuMasterForm) throws Exception {

        Gson gson = new Gson();

        logger.info("{} getLeftMenuList()", gson.toJson(menuMasterForm));

        String menuName ="";
        String subMenuName ="";
        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            menuName = (String)httpSession.getAttribute("menuName");
            subMenuName = (String)httpSession.getAttribute("subMenuName");
        }

        ResultDto resultDto = menuMasterService.getLeftMenuList(menuMasterForm);
        resultDto.setMenuName(menuName);
        resultDto.setSubMenuName(subMenuName);

        logger.info("getLeftMenuList() resultDto ::: {} ", gson.toJson(resultDto));

        return resultDto;
    }

    /** 메뉴별 계정 권한 - 조회 */
    @RequestMapping(value = "/common/getUserAuthMenu")
    @ResponseBody
    public AuthMenuDto getUserAuthMenu(HttpServletRequest request, HttpServletResponse response, AuthMenuForm authMenuForm) throws Exception {

        AuthMenuDto authMenuDto = new AuthMenuDto();

        Gson gson = new Gson();

        logger.info("getUserAuthMenu() authMenuForm  : {}", gson.toJson(authMenuForm));

        String userId ="";
        HttpSession httpSession = request.getSession(true);
        if(httpSession != null) {
            AdminUser adminUser = (AdminUser) httpSession.getAttribute("accessUser");
            userId = adminUser.getUserId();

            if("admin@mindslab.ai".equals(userId)){
                authMenuDto.setCreateYN("Y");
                authMenuDto.setUpdateYN("Y");
                authMenuDto.setDeleteYN("Y");
            }else{
                int authGroupNo = 0;
                if(adminUser.getAuthGroupNo() != null){
                    authGroupNo = adminUser.getAuthGroupNo();
                }
                authMenuForm.setAuthGroupNo(authGroupNo);
                authMenuForm.setMenuMasterNm((String)httpSession.getAttribute("menuName"));
                authMenuForm.setMenuNm((String)httpSession.getAttribute("subMenuName"));
                authMenuDto = authGroupService.getUserAuthMenu(authMenuForm);
            }
        }

        logger.info("getUserAuthMenu() authMenuDto : {}", gson.toJson(authMenuDto));

        return authMenuDto;
    }
}