package maum.ai.manage.controller.login;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * 로그인 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @GetMapping(value = {"/", "login"})
    public String getLoginPage(HttpServletRequest request, Model model) {
        logger.info("LoginController.{}", "getLoginPage()");

        model.addAttribute("errormsg",request.getParameter("errormsg"));
        return "login/login";
    }

    @GetMapping(value = {"/", "accessDenied"})
    public String getAccessDeniedPage(Model model) {
        logger.info("LoginController.{}", "getAccessDeniedPage()");

        return "login/access-denied";
    }

}