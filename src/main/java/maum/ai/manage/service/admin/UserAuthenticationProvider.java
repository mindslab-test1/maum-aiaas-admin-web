package maum.ai.manage.service.admin;

import maum.ai.manage.domain.user.UserPrincipal;
import maum.ai.manage.entity.AdminUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

/**
 * 스프링시큐리티 사용자 권한 및 인증 처리를 위한 Provider
 *
 * @author unongko
 * @version 1.0
 */

@Transactional
public class UserAuthenticationProvider extends DaoAuthenticationProvider {

    private static final Logger logger = LoggerFactory.getLogger(UserAuthenticationProvider.class);
    
    @Autowired
    private AdminUserService adminUserService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        AdminUser adminUser = new AdminUser();
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        logger.info("UserAuthenticationProvider() username : {} " , username);
//        logger.info("UserAuthenticationProvider() password : {} " , password);

        UserPrincipal userPrincipal = new UserPrincipal();

        if(!"".equals(username) && username != null){
            userPrincipal = (UserPrincipal) adminUserService.loadUserByUsername(username);
            if(userPrincipal == null){
                throw new UsernameNotFoundException(username);
            }
        }else{
            throw new UsernameNotFoundException(username);
        }


        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        adminUser.setUserId(username);

        //비밀번호 확인
        if(!passwordEncoder.matches(password, userPrincipal.getPassword())) {
            try{
                logger.info("UserAuthenticationProvider() userPrincipal.getLoginFailCnt() : {} " , userPrincipal.getLoginFailCnt());
                //계정상태 잠금
                if(userPrincipal.isEnabled() && userPrincipal.getLoginFailCnt() >= 4) {
                    logger.info("UserAuthenticationProvider()updateMemberEnabled");
                    adminUserService.updateAdminUserEnabled(adminUser);
                }
                //로그인실패횟수 증가
                if(userPrincipal.isEnabled() && userPrincipal.getLoginFailCnt() < 5){
                    adminUser.setLoginFailCNT(userPrincipal.getLoginFailCnt()+1);
                    adminUserService.updateAdminUserFailCnt(adminUser);
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            //계정상태 확인
            if(!userPrincipal.isEnabled()) {
                logger.info("UserAuthenticationProvider() LockedException ");
                throw new LockedException(username);
            }else{
                throw new BadCredentialsException(username);
            }
        }

        //계정상태 확인
        if(!userPrincipal.isEnabled()) {
            logger.info("UserAuthenticationProvider() LockedException");
            throw new LockedException(username);
        }

        return new UsernamePasswordAuthenticationToken(username, password, userPrincipal.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
 
}