package maum.ai.manage.service.admin;

import com.google.gson.Gson;
import maum.ai.manage.domain.admin.*;
import maum.ai.manage.domain.common.CommonDto;
import maum.ai.manage.domain.common.CommonForm;
import maum.ai.manage.domain.common.MenuAuthRoleEnum;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.mapper.AuthGroupMapper;
import maum.ai.manage.util.PagingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * 권한그룹 비지니스로직 서비스
 *
 * @author unongko
 * @version 1.0
 */

@Transactional
@Service
public class AuthGroupService {

	private static final Logger logger = LoggerFactory.getLogger(AuthGroupService.class);

	@Autowired
	private AuthGroupMapper authGroupMapper;

	/** 권한 - 목록 조회 */
	public ResultDto getAuthGroupList(AuthGroupForm authGroupForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		CommonDto commonDto = new CommonDto();

		int totalCount = authGroupMapper.getAuthGroupCnt(authGroupForm);
		if (totalCount != 0) {
			CommonForm commonForm = new CommonForm();
			commonForm.setFunction_name(authGroupForm.getFunction_name());
			commonForm.setCurrent_page_no(authGroupForm.getCurrent_page_no());
			commonForm.setCount_per_page(10);
			if(authGroupForm.getCount_per_list() == null){
				commonForm.setCount_per_list(10);
			}else{
				commonForm.setCount_per_list(authGroupForm.getCount_per_list());
			}
			commonForm.setTotal_list_count(totalCount);
			commonDto = PagingUtil.setPageUtil(commonForm);

			authGroupForm.setLimit(commonDto.getLimit());
			authGroupForm.setOffset(commonDto.getOffset());
		}else{
			authGroupForm.setLimit(10);
			authGroupForm.setOffset(0);
		}


		List<AuthGroupDto> list = authGroupMapper.getAuthGroupList(authGroupForm);
		

		Gson gson = new Gson();
		logger.info("AuthGroupDto list : {} " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		resultMap.put("totalCount", totalCount);
		resultMap.put("pagination", commonDto.getPagination());

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	/** 메뉴 권한 - 저장 */
	public AuthGroupDto mergeAuthGroup(AuthGroupForm authGroupForm) throws Exception {

		AuthGroupDto authGroupDto = new AuthGroupDto();

		int insertCnt = 0;

		// 메뉴권한 저장
		insertCnt = authGroupMapper.mergeAuthGroup(authGroupForm);

		if (insertCnt > 0) {
			authGroupDto.setResult("SUCCESS");
		} else {
			authGroupDto.setResult("FAIL");
		}

		return authGroupDto;
	}

	/** 메뉴 권한 - 단건 삭제 */
	public AuthGroupDto deleteMultiAuthGroup(AuthGroupForm authGroupForm) throws Exception {

		AuthGroupDto authGroupDto = new AuthGroupDto();
		Gson gson = new Gson();
		logger.info("AuthGroupService.deleteMultiAuthGroup() : {} " , gson.toJson(authGroupForm));
		logger.info("authGroupForm.getAuthGroupNoArr().length : {} " , authGroupForm.getAuthGroupNoArr().length);

		int deleteCnt = 0;

		if(authGroupForm.getAuthGroupNoArr() != null) {
			for(int i=0; i < authGroupForm.getAuthGroupNoArr().length; i++) {
				authGroupForm.setAuthGroupNo(authGroupForm.getAuthGroupNoArr()[i]);
				deleteCnt = authGroupMapper.deleteAuthGroup(authGroupForm);
			}
		}

		if (deleteCnt > 0) {
			authGroupDto.setResult("SUCCESS");
		} else {
			authGroupDto.setResult("FAIL");
		}

		return authGroupDto;
	}

	/** 권한그룹 상세 조회 */
	public AuthGroupDto getAuthGroupDetail(Integer authGroupNo) throws Exception {

		AuthGroupDto authGroupDto = new AuthGroupDto();

		authGroupDto = authGroupMapper.getAuthGroupDetail(authGroupNo);

		return authGroupDto;
	}

	/** 권한메뉴 - 저장 */
	public AuthMenuDto mergeAuthMenu(AuthMenuForm authMenuForm) throws Exception {

		AuthMenuDto authMenuDto = new AuthMenuDto();

		int insertCnt = 0;

		// 등록된 메뉴 확인
		int authMenuNo = 0;
		try{
			authMenuNo = authGroupMapper.getAuthMenuCheck(authMenuForm);
		}catch (Exception e){
			logger.info("getAuthMenuCheck Null Exception ::: {}");
		}

		logger.info("authMenuNo ::: {}", authMenuNo);
		logger.info("authMenuForm.getAuthMenuNo() ::: {}", authMenuForm.getAuthMenuNo());

		if(authMenuNo != 0 &&  authMenuForm.getAuthMenuNo() == 0){
			authMenuForm.setAuthMenuNo(authMenuNo);
			authMenuForm.setUpdateUser(authMenuForm.getCreateUser());
		}

		// 메뉴권한 저장
		insertCnt = authGroupMapper.mergeAuthMenu(authMenuForm);

		if (insertCnt > 0) {
			authMenuDto.setResult("SUCCESS");
		} else {
			authMenuDto.setResult("FAIL");
		}

		return authMenuDto;
	}

	/** 권한메뉴 - 목록 조회 */
	public ResultDto getAuthMenuList(AuthMenuForm authMenuForm) throws Exception {

		ResultDto resultDto = new ResultDto();
		List<AuthMenuDto> list = authGroupMapper.getAuthMenuList(authMenuForm);

		int i=0;
		for(AuthMenuDto authMenuDto : list) {
			list.get(i).setRoleCodeNm(MenuAuthRoleEnum.valueOf(authMenuDto.getRoleCode()).getCodeNm());
			i++;
		}

		Gson gson = new Gson();
		logger.info("Service getAuthMenuList : {} " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	/** 권한메뉴 - 다건 삭제 */
	public AuthMenuDto deleteMultiAuthMenu(AuthMenuForm authMenuForm) throws Exception {

		AuthMenuDto authMenuDto = new AuthMenuDto();
		Gson gson = new Gson();
		logger.info("Service.deleteMultiAuthMenu() : {} " , gson.toJson(authMenuForm));

		int deleteCnt = 0;

		if(authMenuForm.getAuthMenuNoArr() != null) {
			for(int i=0; i < authMenuForm.getAuthMenuNoArr().length; i++) {
				authMenuForm.setAuthMenuNo(authMenuForm.getAuthMenuNoArr()[i]);
				deleteCnt = authGroupMapper.deleteAuthMenu(authMenuForm);
			}
		}

		if (deleteCnt > 0) {
			authMenuDto.setResult("SUCCESS");
		} else {
			authMenuDto.setResult("FAIL");
		}

		return authMenuDto;
	}

	/** 권한메뉴 상세 조회 */
	public AuthMenuDto getAuthMenuDetail(Integer authMenuNo) throws Exception {

		AuthMenuDto authMenuDto = new AuthMenuDto();

		authMenuDto = authGroupMapper.getAuthMenuDetail(authMenuNo);

		return authMenuDto;
	}

	/** 권한그룹 - selectBox 목록 조회 */
	public ResultDto getAuthGroupSelectList(AuthGroupForm authGroupForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		List<AuthGroupDto> list = authGroupMapper.getAuthGroupSelectList(authGroupForm);

		Gson gson = new Gson();
		logger.info("AuthGroupDto list : {} " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	/** 메뉴별 계정 권한 - 조회 */
	public AuthMenuDto getUserAuthMenu(AuthMenuForm authMenuForm) throws Exception {

		AuthMenuDto authMenuDto = new AuthMenuDto();

		authMenuDto = authGroupMapper.getUserAuthMenu(authMenuForm);

		return authMenuDto;
	}
}
