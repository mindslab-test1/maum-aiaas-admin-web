package maum.ai.manage.service.admin;

import com.google.gson.Gson;
import maum.ai.manage.domain.admin.AdminUserDto;
import maum.ai.manage.domain.admin.AdminUserForm;
import maum.ai.manage.domain.common.CommonDto;
import maum.ai.manage.domain.common.CommonForm;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.domain.user.UserDto;
import maum.ai.manage.domain.user.UserPrincipal;
import maum.ai.manage.entity.AdminUser;
import maum.ai.manage.mapper.AdminUserMapper;
import maum.ai.manage.util.PagingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * 관리사용자 비지니스로직 서비스
 *
 * @author unongko
 * @version 1.0
 */

@Transactional
@Service
public class AdminUserService implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(AdminUserService.class);

	@Autowired
	private AdminUserMapper adminUserMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

	public UserPrincipal findUserByLoginId(String loginId) {
		return adminUserMapper.findUserByLoginId(loginId);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserPrincipal userPrincipal = adminUserMapper.findUserByLoginId(username);
		return userPrincipal;
	}

	public AdminUser getLoginAdminUserDetail(String userId) throws Exception {

		AdminUser adminUser = new AdminUser();

		adminUser = adminUserMapper.getLoginAdminUserDetail(userId);

		return adminUser;
	}

	public AdminUserDto updateAdminUserEnabled(AdminUser adminUser) throws Exception {

		AdminUserDto adminUserDto = new AdminUserDto();

		int updateCnt = adminUserMapper.updateAdminUserEnabled(adminUser);

		if (updateCnt > 0) {
			adminUserDto.setResult("SUCCESS");
		} else {
			adminUserDto.setResult("FAIL");
		}

		return adminUserDto;
	}

	public UserDto updateAdminUserFailCnt(AdminUser adminUser) throws Exception {

		UserDto userDto = new UserDto();

		int updateCnt = adminUserMapper.updateAdminUserFailCnt(adminUser);

		if (updateCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}


	public AdminUserDto updateAdminUserPwd(AdminUser adminUser) throws Exception {

		AdminUserDto adminUserDto = new AdminUserDto();

		if(!"".contentEquals(adminUser.getPassword())) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			adminUser.setPassword(passwordEncoder.encode(adminUser.getPassword()));
		}

		int updateCnt = adminUserMapper.updateAdminUserPwd(adminUser);

		if (updateCnt > 0) {
			adminUserDto.setResult("SUCCESS");
		} else {
			adminUserDto.setResult("FAIL");
		}

		return adminUserDto;
	}


	/** 계정 - 목록 조회 */
	public ResultDto getAdminUserList(AdminUserForm adminUserForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		CommonDto commonDto = new CommonDto();

		int totalCount = adminUserMapper.getAdminUserCnt(adminUserForm);
		if (totalCount != 0) {
			CommonForm commonForm = new CommonForm();
			commonForm.setFunction_name(adminUserForm.getFunction_name());
			commonForm.setCurrent_page_no(adminUserForm.getCurrent_page_no());
			commonForm.setCount_per_page(10);
			if(adminUserForm.getCount_per_list() == null){
				commonForm.setCount_per_list(10);
			}else{
				commonForm.setCount_per_list(adminUserForm.getCount_per_list());
			}
			commonForm.setTotal_list_count(totalCount);
			commonDto = PagingUtil.setPageUtil(commonForm);

			adminUserForm.setLimit(commonDto.getLimit());
			adminUserForm.setOffset(commonDto.getOffset());
		}else{
			adminUserForm.setLimit(10);
			adminUserForm.setOffset(0);
		}


		List<AdminUserDto> list = adminUserMapper.getAdminUserList(adminUserForm);

		Gson gson = new Gson();
		logger.info("AdminUserDto list : {} " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		resultMap.put("totalCount", totalCount);
		resultMap.put("pagination", commonDto.getPagination());

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	public AdminUserDto getAdminUserDetail(Integer userNo) throws Exception {

		AdminUserDto adminUserDto = new AdminUserDto();

		adminUserDto = adminUserMapper.getAdminUserDetail(userNo);

		return adminUserDto;
	}

	public AdminUserDto updateAdminUserInfo(AdminUser adminUser) throws Exception {

		AdminUserDto adminUserDto = new AdminUserDto();
		if(!"".contentEquals(adminUser.getPassword())) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			adminUser.setPassword(passwordEncoder.encode(adminUser.getPassword()));
		}

		int updateCnt = adminUserMapper.updateAdminUserInfo(adminUser);

		if (updateCnt > 0) {
			adminUserDto.setResult("SUCCESS");
		} else {
			adminUserDto.setResult("FAIL");
		}

		return adminUserDto;
	}

	public AdminUserDto getAdminUserId(String userId) throws Exception {

		AdminUserDto adminUserDto = new AdminUserDto();

		adminUserDto = adminUserMapper.getAdminUserId(userId);

		return adminUserDto;
	}

	/** 계정 - 등록 */
	public AdminUserDto insertAdminUser(AdminUserForm adminUserForm) throws Exception {

		AdminUserDto adminUserDto = new AdminUserDto();

		int insertCnt = 0;

		adminUserForm.setAuthority(adminUserForm.getAuthority());

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		adminUserForm.setPassword(passwordEncoder.encode(adminUserForm.getPassword()));

		insertCnt = adminUserMapper.insertAdminUser(adminUserForm);

		if (insertCnt > 0) {
			adminUserDto.setResult("SUCCESS");
		} else {
			adminUserDto.setResult("FAIL");
		}

		return adminUserDto;
	}

	/** 계정 - 단건 삭제 */
	public AdminUserDto deleteMultiAdminUser(AdminUserForm adminUserForm) throws Exception {

		AdminUserDto adminUserDto = new AdminUserDto();
		Gson gson = new Gson();
		logger.info("AdminUserService.deleteMultiAdminUser() : {} " , gson.toJson(adminUserForm));
		logger.info("adminUserForm.getAdminUserNoArr().length : {} " , adminUserForm.getAdminUserNoArr().length);

		int deleteCnt = 0;

		if(adminUserForm.getAdminUserNoArr() != null) {
			for(int i=0; i < adminUserForm.getAdminUserNoArr().length; i++) {
				adminUserForm.setUserNo(adminUserForm.getAdminUserNoArr()[i]);
				deleteCnt = adminUserMapper.deleteAdminUser(adminUserForm);
			}
		}

		if (deleteCnt > 0) {
			adminUserDto.setResult("SUCCESS");
		} else {
			adminUserDto.setResult("FAIL");
		}

		return adminUserDto;
	}

	/** 메뉴권한 사용자 - 목록 조회 */
	public ResultDto getMenuAuthAdminUserList(AdminUserForm adminUserForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		List<AdminUserDto> list = adminUserMapper.getMenuAuthAdminUserList(adminUserForm);

		Gson gson = new Gson();
		logger.info("AdminUserDto list : {} " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}
}
