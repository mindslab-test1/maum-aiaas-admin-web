package maum.ai.manage.service.admin;

import com.google.gson.Gson;
import maum.ai.manage.domain.admin.MenuDto;
import maum.ai.manage.domain.admin.MenuForm;
import maum.ai.manage.domain.admin.MenuMasterDto;
import maum.ai.manage.domain.admin.MenuMasterForm;
import maum.ai.manage.domain.common.CommonDto;
import maum.ai.manage.domain.common.CommonForm;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.mapper.MenuMasterMapper;
import maum.ai.manage.util.PagingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * MenuMaster 비지니스로직 서비스
 *
 * @author unongko
 * @version 1.0
 */

@Transactional
@Service
public class MenuMasterService {

	private static final Logger logger = LoggerFactory.getLogger(MenuMasterService.class);

	@Autowired
	private MenuMasterMapper menuMasterMapper;

	/** 메뉴 - 목록 조회 */
	public ResultDto getMenuMasterList(MenuMasterForm menuMasterForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		CommonDto commonDto = new CommonDto();

		int totalCount = menuMasterMapper.getMenuMasterCnt(menuMasterForm);
		if (totalCount != 0) {
			CommonForm commonForm = new CommonForm();
			commonForm.setFunction_name(menuMasterForm.getFunction_name());
			commonForm.setCurrent_page_no(menuMasterForm.getCurrent_page_no());
			commonForm.setCount_per_page(10);
			commonForm.setCount_per_list(menuMasterForm.getCount_per_list());
			commonForm.setTotal_list_count(totalCount);
			commonDto = PagingUtil.setPageUtil(commonForm);

			menuMasterForm.setLimit(commonDto.getLimit());
			menuMasterForm.setOffset(commonDto.getOffset());
		}else{
			menuMasterForm.setLimit(10);
			menuMasterForm.setOffset(0);
		}

		List<MenuMasterDto> list = menuMasterMapper.getMenuMasterList(menuMasterForm);

		Gson gson = new Gson();
		logger.info("getMenuMasterList list : {} " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		resultMap.put("totalCount", totalCount);
		resultMap.put("pagination", commonDto.getPagination());

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	/** 메뉴마스터코드 - 목록 조회 */
	public ResultDto getMenuMasterCodeList(MenuMasterForm menuMasterForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		List<MenuMasterDto> list = menuMasterMapper.getMenuMasterCodeList(menuMasterForm);

		Gson gson = new Gson();
		logger.info("getMenuMasterCodeList list : {} " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	public MenuMasterDto getMenuMasterDetail(Integer menuMasterNo) throws Exception {

		MenuMasterDto menuMasterDto = new MenuMasterDto();

		menuMasterDto = menuMasterMapper.getMenuMasterDetail(menuMasterNo);

		return menuMasterDto;
	}

	/** 메인메뉴 - 저장 */
	public MenuMasterDto mergeMenuMaster(MenuMasterForm menuMasterForm) throws Exception {

		MenuMasterDto menuMasterDto = new MenuMasterDto();

		int insertCnt = 0;

		insertCnt = menuMasterMapper.mergeMenuMaster(menuMasterForm);

		if (insertCnt > 0) {
			menuMasterDto.setResult("SUCCESS");
		} else {
			menuMasterDto.setResult("FAIL");
		}

		return menuMasterDto;
	}

	/** 메인메뉴 - 단건 삭제 */
	public MenuMasterDto deleteMultiMenuMaster(MenuMasterForm menuMasterForm) throws Exception {

		MenuMasterDto menuMasterDto = new MenuMasterDto();
		Gson gson = new Gson();
		logger.info("MenuMasterService.deleteMultiMenuMaster() : {} " , gson.toJson(menuMasterForm));

		int deleteCnt = 0;

		if(menuMasterForm.getMenuMasterNoArr() != null) {
			for(int i=0; i < menuMasterForm.getMenuMasterNoArr().length; i++) {
				menuMasterForm.setMenuMasterNo(menuMasterForm.getMenuMasterNoArr()[i]);
				deleteCnt = menuMasterMapper.deleteMenuMaster(menuMasterForm);
			}
		}

		if (deleteCnt > 0) {
			menuMasterDto.setResult("SUCCESS");
		} else {
			menuMasterDto.setResult("FAIL");
		}

		return menuMasterDto;
	}

	/** 서브메뉴 - 목록 조회 */
	public ResultDto getMenuList(MenuForm menuForm) throws Exception {

		ResultDto resultDto = new ResultDto();
		List<MenuDto> list = menuMasterMapper.getMenuList(menuForm);

		Gson gson = new Gson();
		logger.info("MenuMasterService getMenuList : {} " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	/** 서브메뉴코드 - 목록 조회 */
	public ResultDto getMenuCodeList(MenuForm menuForm) throws Exception {

		ResultDto resultDto = new ResultDto();
		List<MenuDto> list = menuMasterMapper.getMenuCodeList(menuForm);

		Gson gson = new Gson();
		logger.info("MenuMasterService getMenuCodeList : {} " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	/** 서브메뉴 - 저장 */
	public MenuDto mergeMenu(MenuForm menuForm) throws Exception {

		MenuDto menuDto = new MenuDto();

		int insertCnt = 0;

		insertCnt = menuMasterMapper.mergeMenu(menuForm);

		if (insertCnt > 0) {
			menuDto.setResult("SUCCESS");
		} else {
			menuDto.setResult("FAIL");
		}

		return menuDto;
	}

	public MenuDto getMenuDetail(Integer menuNo) throws Exception {

		MenuDto menuDto = new MenuDto();

		menuDto = menuMasterMapper.getMenuDetail(menuNo);

		return menuDto;
	}

	/** 서브메뉴 - 단건 삭제 */
	public MenuDto deleteMultiMenu(MenuForm menuForm) throws Exception {

		MenuDto menuDto = new MenuDto();
		Gson gson = new Gson();
		logger.info("MenuMasterService.deleteMultiMenu() : {} " , gson.toJson(menuForm));

		int deleteCnt = 0;

		if(menuForm.getMenuNoArr() != null) {
			for(int i=0; i < menuForm.getMenuNoArr().length; i++) {
				menuForm.setMenuNo(menuForm.getMenuNoArr()[i]);
				deleteCnt = menuMasterMapper.deleteMenu(menuForm);
			}
		}

		if (deleteCnt > 0) {
			menuDto.setResult("SUCCESS");
		} else {
			menuDto.setResult("FAIL");
		}

		return menuDto;
	}

	/** LeftMenu - 목록 조회 */
	public ResultDto getLeftMenuList(MenuMasterForm menuMasterForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		List<MenuMasterDto> list = menuMasterMapper.getLeftMenuMasterList(menuMasterForm);

		int i=0;
		MenuForm menuForm = new MenuForm();
		for(MenuMasterDto menuMasterDto : list) {
			menuForm.setMenuMasterNo(menuMasterDto.getMenuMasterNo());
			list.get(i).setMenuSubList(menuMasterMapper.getLeftMenuSubList(menuForm));
			i++;
		}

		Gson gson = new Gson();
		logger.info("MenuMasterService getLeftMenuList : {} ", gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}
}
