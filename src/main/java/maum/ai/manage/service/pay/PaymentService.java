package maum.ai.manage.service.pay;

import com.google.gson.Gson;
import com.inicis.inipay4.INIpay;
import com.inicis.inipay4.util.INIdata;
import maum.ai.manage.domain.common.CommonDto;
import maum.ai.manage.domain.common.CommonForm;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.domain.payment.PayDto;
import maum.ai.manage.domain.payment.PayForm;
import maum.ai.manage.domain.payment.PayCsvForm;
import maum.ai.manage.entity.Pay;
import maum.ai.manage.mapper.PaymentMapper;
import maum.ai.manage.util.PagingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class PaymentService {

    private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);

    @Autowired
    private PaymentMapper paymentMapper;

    public List<Integer> getNumberOfCustomer(){

        return paymentMapper.numberOfCustomer();
    }

    /** 결제 내역 조회 */
    public ResultDto getPayList(PayForm payForm) throws Exception {

        ResultDto resultDto = new ResultDto();

        CommonDto commonDto = new CommonDto();

        int totalCount = paymentMapper.getPayCnt(payForm);

        logger.debug("count search pay : {}", totalCount);

        if (totalCount != 0) {
            CommonForm commonForm = new CommonForm();
            commonForm.setFunction_name(payForm.getFunction_name());
            commonForm.setCurrent_page_no(payForm.getCurrent_page_no());
            commonForm.setCount_per_page(10);
            commonForm.setCount_per_list(payForm.getCount_per_list());
            commonForm.setTotal_list_count(totalCount);
            commonDto = PagingUtil.setPageUtil(commonForm);

            payForm.setLimit(commonDto.getLimit());
            payForm.setOffset(commonDto.getOffset());
        }else{
            payForm.setLimit(10);
            payForm.setOffset(0);
        }

        List<PayDto> list = paymentMapper.getPayList(payForm);

        List<PayCsvForm> csvList = paymentMapper.getPayListForCsv(payForm);
        PayCsvForm csvForm =  new PayCsvForm();
        csvForm.makeTableHead();
        csvList.add(0,csvForm);

        Gson gson = new Gson();
        logger.debug("PayDto list ======= {}", gson.toJson(list));

        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("list", list);
        resultMap.put("totalCount", totalCount);
        resultMap.put("pagination", commonDto.getPagination());
        resultMap.put("CSV", csvList);

        resultDto.setData(resultMap);
        resultDto.setState("SUCCESS");

        return resultDto;
    }


    public List<PayDto> getTodayPayList(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date time = new Date();
        String currentTime = format.format(time);

        List<PayDto> payList, payFailList, totalPay = new ArrayList<>();
        payList = paymentMapper.todayPayList(currentTime);
        payFailList = paymentMapper.payFailList();

        int num=1;

        for(PayDto list : payList){
            list.setStatus("승인");
            list.setNum(num);
            num++;
        }

        for(PayDto failList : payFailList){
            failList.setStatus("실패");
            failList.setPayDate(failList.getPaymentDate());
            failList.setNum(num);
            num++;
        }

        totalPay.addAll(payList);
        totalPay.addAll(payFailList);

        return totalPay;
    }

    public Map<String, Object> getMAUCount(){
        final String DATE_PATTERN = "yyyy-MM-dd";
        Map<String, Object> resultMap = new HashMap<>();


        try {
            //현재 시간
            LocalDateTime nowDateTime = LocalDateTime.now();
            String nowTime = nowDateTime.toString().split("T")[1];

            SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
            Date currentDate = new Date();

            if(Integer.parseInt(nowTime.split(":")[0]) < 12){
                Calendar dayAgo = Calendar.getInstance();
                dayAgo.setTime(currentDate);
                dayAgo.add(Calendar.DAY_OF_MONTH, -1);
                currentDate = dayAgo.getTime();
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            cal.add(Calendar.DAY_OF_MONTH, -13);
            Date startDate = cal.getTime();

            String startDateFormat = sdf.format(startDate);

            logger.debug("startDateFormat {}", startDateFormat);

            int preMAU;

            if(paymentMapper.MAUNullCheck(startDateFormat) != 0) {
                preMAU = paymentMapper.MAUCount(startDateFormat);
            }else{
                preMAU = 0;
            }
            // int preMAU = 3;
            List<PayDto> resultPay = new ArrayList<>();

            PayDto payDto = new PayDto();
            payDto.setDay(startDateFormat.split("-")[1]+"/"+startDateFormat.split("-")[2]);
            payDto.setMAU(preMAU);
            resultPay.add(payDto);


            while(startDate.compareTo(currentDate) < 0){
                PayDto payDto2 = new PayDto();
                cal.setTime(startDate);
                cal.add(Calendar.DAY_OF_MONTH, 1);

                startDateFormat = sdf.format(cal.getTime());

                int MAU;
                if(paymentMapper.MAUNullCheck(startDateFormat) != 0) {
                    MAU = paymentMapper.MAUCount(startDateFormat);
                }else{
                    MAU = 0;
                }

                if(preMAU<MAU){
                    payDto2.setUpdown(1);
                    payDto2.setDif(MAU-preMAU);
                }
                else if(preMAU>MAU){
                    payDto2.setUpdown(-1);
                    payDto2.setDif(preMAU-MAU);
                }

                payDto2.setDay(startDateFormat.split("-")[1]+"/"+startDateFormat.split("-")[2]);
                payDto2.setMAU(MAU);

                resultPay.add(payDto2);
                startDate = cal.getTime();
                preMAU = MAU;
            }

            resultMap.put("data", resultPay);
            System.out.println("resultMap : "+resultMap);

        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return resultMap;
    }

    public List<Integer> getSales(){
        final String DATE_PATTERN = "yyyy-MM-dd";
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        String currentDate = dateFormat.format(date);

        int year = Integer.parseInt(currentDate.split("-")[0]);
        int month = Integer.parseInt(currentDate.split("-")[1]);
        Calendar cal = Calendar.getInstance();

        cal.set(year, month-1, 1);
        String startDate = dateFormat.format(cal.getTime());

        cal.set(year, month-1, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        String endDate = dateFormat.format(cal.getTime());

        PayForm payForm = new PayForm();
        payForm.setCurrentDate(currentDate);
        payForm.setStartDate(startDate);
        payForm.setEndDate(endDate);

        return paymentMapper.numberOfPayment(payForm);
    }


    public ResultDto payCancel(HttpServletRequest request, int sessionUser, String tid, String payment){

        ResultDto resultDto = new ResultDto();
        Pay pay;

        try{
            pay = paymentMapper.getPayInfo(tid);
            pay.setUserId(sessionUser);
            logger.debug("pay_T :: {}",pay);
        }catch (Exception e){
            logger.error("getPayInfo ERROR :: {0}", e);
            resultDto.setState("FAIL");
            resultDto.setMsg("getPayInfo ERROR");
            return resultDto;
        }


        if(payment.equals("PayPal")){
            // TODO 페이팔 결제 취소 API 조사 후 수정 필요
        }
        else{
            iniPayCancel(request, pay, resultDto);
        }

        logger.info("resultDto: {}",resultDto);
        return resultDto;
    }



    @Value("${file.inipayHome}")
    private String inipayHome;

    public void iniPayCancel(HttpServletRequest request, Pay pay, ResultDto resultDto){

        String uip = this.getUip(request);
        String tid = pay.getTid();
        INIpay inipay = new INIpay();
        INIdata data = new INIdata();

        data.setData("inipayHome", inipayHome);         // INIpay45 절대경로 (key / log 디렉토리) 추후 위치 수정 예정
        data.setData("logMode", "INFO");               // 로그모드 (INFO / DEBUG)
        data.setData("type", "cancel");                 // type (고정)
        data.setData("crypto", "execure");
        data.setData("uip", uip);
//        data.setData("url", "https://maum.ai");
        data.setData("mid", "mindslab01");
//        data.setData("uid", "mindslab01");
        data.setData("keyPW", "1111");
        data.setData("tid", tid);

        INIdata resultData = inipay.payRequest(data);

        logger.info("결제 취소 결과 : {}", resultData);

        String resultCode = resultData.getData("ResultCode");
        String resultMsg = resultData.getData("ResultMsg");

        HashMap<String, Object> map = new HashMap<>();

        try{
            if(resultCode.equals("00")){
                logger.info("이니시스 결제 취소 성공");

                map.put("tid", tid);
                map.put("status", 0);
                map.put("type", "cancel");
                map.put("failReason", null);

                paymentMapper.updatePayTtoCancel(pay); // pay_T update
                paymentMapper.insertLogPay(map); // LogPay insert

                resultDto.setState("SUCCESS");
                resultDto.setMsg(resultMsg);

            }
            else{
                logger.error("이니시스 결제 취소 실패 :: {}", resultMsg);

                map.put("tid", tid);
                map.put("status", -1);
                map.put("type", "cancel");
                map.put("failReason", resultMsg);

                paymentMapper.insertLogPay(map); // LogPay insert

                resultDto.setState("FAIL");
                resultDto.setMsg(resultMsg);
            }
        }
        catch (Exception e){

        }
    }

    public String getUip(HttpServletRequest request) {

        String uip = request.getHeader("X-FORWARDED-FOR");

        if (uip == null) {
            uip = request.getHeader("Proxy-Client-IP");
        }

        if (uip == null) {
            uip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (uip == null) {
            uip = request.getHeader("HTTP_CLIENT_IP");
        }

        if (uip == null) {
            uip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (uip == null) {
            uip = request.getRemoteAddr();
        }

        return uip;
    }
}
