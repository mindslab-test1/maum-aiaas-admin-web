package maum.ai.manage.service.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import maum.ai.manage.domain.user.UserDto;
import maum.ai.manage.entity.User;
import maum.ai.manage.mapper.UserMapper;
import maum.ai.manage.service.user.UserService;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class ApiKeyService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;


    /* api 서버가 불안정하여 api 2.0 적용 전까지 dev를 찌르기로 유병욱T와 협의 -- 2020-12-11 LYJ */
    @Value("${api.server.url}")
    private String apiServerUrl;
    @Value("${api.server.path.account.create}")
    private String apiPathCreate;
    @Value("${api.server.path.account.delete}")
    private String apiPathDelete;
    @Value("${api.server.path.account.resume}")
    private String apiPathResume;

    private static final Logger logger = LoggerFactory.getLogger(ApiKeyService.class);


    /* API 계정 pause -- 2020.12.11 LYJ
               modify -- 2020.12.24 YGE */
    public Map<String, String> createAPI(UserDto userDto) {
        logger.debug("-- start create API --");
        Map<String, String> resultMap;

        String apiUrl = apiServerUrl + apiPathCreate;
        String email = userDto.getEmail();
        String apiId = makeNewApiId(email);

        Map<String, String> usrMap = new HashMap<>();
        usrMap.put("apiId", apiId);
        usrMap.put("email", email);
        usrMap.put("name", userDto.getName());

        logger.info(usrMap.toString());

        /* api request */
        HttpURLConnection conn = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonValue = mapper.writeValueAsString(usrMap);

            URL url = new URL(apiUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");

            OutputStream os = conn.getOutputStream();
            os.write(jsonValue.getBytes("UTF-8"));
            os.flush();

            int nResponseCode = conn.getResponseCode();
            logger.info("createAPI responseCode : " + nResponseCode);

            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
            StringBuilder result = new StringBuilder();
            String inputLine;

            while ((inputLine = br.readLine()) != null) {
                try {
                    result.append((URLDecoder.decode(inputLine, "UTF-8")));
                } catch (Exception ex) {
                    result.append(inputLine);
                }
            }

            logger.info("Response Result : {}", result);
            br.close();


            /* api 등록 */
            if (nResponseCode == HttpURLConnection.HTTP_OK) { // 200 성공
                JsonParser parser = new JsonParser();
                JsonObject apiJson = (JsonObject)parser.parse(result.toString());
                String message = apiJson.get("message").toString().replace("\"", "");


                /* db에 api 값 저장 */
                if (message.equals("Success")) {
                    /* apiKey 가져옴 */
                    String apiKey = apiJson.getAsJsonObject("data").get("apiKey").toString().replace("\"", "");

                    User user = new User();
                    user.setApiId(apiId);
                    user.setApiKey(apiKey);
                    user.setEmail(email);

                    logger.info(user.toString());

                    // update database
                    if (userMapper.updateUserApiInfo(user) > 0) {
                        logger.debug("Create API success.");
                        resultMap = userService.setResultMap("0", "Create API success.");
                    } else {
                        logger.error("Create API success, but failed to update DB with new api info.");
                        resultMap = userService.setResultMap("1", "Create API success(respCode: "+ nResponseCode +"), but failed to update DB with new api info.");
                    }

                } else {
                    logger.error("Create API response success, but response message is not success.");
                    resultMap = userService.setResultMap("1", "Create API response success(respCode: "+ nResponseCode +"), but response message is not success.");
                }

                return resultMap;

            } else { // response error
                logger.error("Create API request fail");
                resultMap = userService.setResultMap("1", "Create API request fail, responseCode : " + nResponseCode);
            }

        } catch (Exception e) {
            logger.error("Create API exception : " + e.getMessage());
            resultMap = userService.setResultMap("1", e.getMessage());

        }finally {
            if (conn != null) { conn.disconnect(); }
        }

        return resultMap;
    }


    public String makeNewApiId(String email){
        String apiId;
        int isDuplicate;

        do {
            UUID uuid = UUID.randomUUID();
            apiId = email.split("@")[0] + uuid.toString().split("-")[0] + uuid.toString().split("-")[1];
            isDuplicate = userMapper.getApiId(apiId);
            logger.debug("Duplicate Check :: " + apiId + " :: isDuplicate? " + (isDuplicate == 1));
        }while (isDuplicate != 0);

        return apiId;
    }


    /* API 계정 pause -- 2020.12.24 YGE */
    public Map<String, String> pauseApiId(String apiId) {
        logger.debug("-- start pause API --");
        Map<String, String> resultMap;

        String apiUrl = apiServerUrl + apiPathDelete;
        Map<String, String> userApi = new HashMap<>();
        userApi.put("apiId", apiId); // apiId가 없는 경우는 UserService.updateUserStatus에서 처리

        HttpURLConnection conn = null;
        try{
            URL url = new URL(apiUrl);
            conn = (HttpURLConnection) url.openConnection();
            ObjectMapper mapper = new ObjectMapper();
            String jsonValue = mapper.writeValueAsString(userApi);
            conn.setDoOutput(true);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");

            OutputStream os = conn.getOutputStream();
            os.write(jsonValue.getBytes("UTF-8"));
            os.flush();

            int nResponseCode = conn.getResponseCode();
            logger.info("pauseApi responseCode : " + nResponseCode);

            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
            StringBuilder result = new StringBuilder();
            String inputLine;

            while ((inputLine = br.readLine()) != null) {
                try {
                    result.append((URLDecoder.decode(inputLine, "UTF-8")));
                } catch (Exception ex) {
                    result.append(inputLine);
                }
            }

            logger.info("Response Result : {}", result);

            if (nResponseCode == HttpURLConnection.HTTP_OK) { //200 성공
                resultMap = userService.setResultMap("0", "Pause API success.");
            } else {
                logger.error("Pause API request fail.");
                resultMap = userService.setResultMap("1", "Pause API request fail, responseCode : " + nResponseCode);
            }
            br.close();

        }
        catch(Exception e){
            logger.error("pause API exception : " + e.getMessage());
            resultMap = userService.setResultMap("1", "pause API exception : " + e.getMessage());
        }
        finally {
            if (conn != null) { conn.disconnect(); }
        }

        return resultMap;
    }


    /* API 계정 resume -- 2020.12.24 YGE */
    public Map<String, String> resumeApiAccount(String apiId){
        logger.debug("-- start resume API --");
        Map<String, String> resultMap;

        String apiUrl = apiServerUrl + apiPathResume;
        HttpClient client = HttpClients.createDefault();
        HttpPatch httpPatch = new HttpPatch(apiUrl);
        httpPatch.addHeader("Content-Type", "application/json");
        httpPatch.addHeader("cache-control", "no-cache");

        try {

            httpPatch.setEntity(new StringEntity("{ \"apiId\" : \"" + apiId + "\"}", "utf-8"));
            HttpResponse response = client.execute(httpPatch);
            int respCode = response.getStatusLine().getStatusCode();
            logger.info("resumeApi responseCode : " + respCode);

            if(respCode >= 200 && respCode < 300){
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder result = new StringBuilder();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }
                logger.info("Response Result : {}", result);

//                JsonParser jsonParser = new JsonParser();
//                String resMsg = jsonParser.parse(result.toString()).getAsJsonObject().get("message").getAsString();
                resultMap = userService.setResultMap("0", "Resume API success.");
            }
            else{
                logger.error("Resume API account fail.");
                resultMap = userService.setResultMap("1", "Resume API account fail, responseCode : " + respCode);
            }
            return resultMap;

        } catch (Exception e) {
            logger.error("Resume API account exception : " + e.toString());
            resultMap = userService.setResultMap("1", "Resume API account exception : " + e.toString());
            return resultMap;
        }

    }
}
