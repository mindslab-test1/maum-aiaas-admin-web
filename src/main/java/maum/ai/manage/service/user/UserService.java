package maum.ai.manage.service.user;

import com.google.gson.Gson;
import maum.ai.manage.domain.common.CommonDto;
import maum.ai.manage.domain.common.CommonForm;
import maum.ai.manage.domain.common.ResultDto;
import maum.ai.manage.domain.user.UserCsvForm;
import maum.ai.manage.domain.user.UserDto;
import maum.ai.manage.domain.user.UserForm;
import maum.ai.manage.entity.Pay;
import maum.ai.manage.entity.User;
import maum.ai.manage.mapper.UserMapper;
import maum.ai.manage.service.api.ApiKeyService;
import maum.ai.manage.util.PagingUtil;
import maum.ai.manage.util.enums.UserStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 사용자 비지니스로직 서비스
 *
 * @author unongko
 * @version 1.0
 */

@Service
public class UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private ApiKeyService apiKeyService;

	/** 계정 - 목록 조회 */
	public ResultDto getUserList(UserForm userForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		CommonDto commonDto = new CommonDto();

		int totalCount = userMapper.getUserCnt(userForm);

		System.out.println("count search user : " + totalCount);

		if (totalCount != 0) {
			CommonForm commonForm = new CommonForm();
			commonForm.setFunction_name(userForm.getFunction_name());
			commonForm.setCurrent_page_no(userForm.getCurrent_page_no());
			commonForm.setCount_per_page(10);
			commonForm.setCount_per_list(userForm.getCount_per_list());
			commonForm.setTotal_list_count(totalCount);
			commonDto = PagingUtil.setPageUtil(commonForm);

			userForm.setLimit(commonDto.getLimit());
			userForm.setOffset(commonDto.getOffset());
		}else{
			userForm.setLimit(10);
			userForm.setOffset(0);
		}

		List<UserDto> list = userMapper.getUserList(userForm);
		List<UserCsvForm> csvList = userMapper.getUserListForCsv(userForm);
		logger.info("UserDto list size : ======= {}", csvList.size());

		UserCsvForm csvForm = new UserCsvForm();
		csvForm.makeTableHead();
		csvList.add(0, csvForm);

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		resultMap.put("totalCount", totalCount);
		resultMap.put("pagination", commonDto.getPagination());
		resultMap.put("CSV", csvList);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	public UserDto getUserDetail(String email) throws Exception {

		UserDto userDto = new UserDto();

		userDto = userMapper.getUserDetail(email);

		return userDto;
	}

	public UserDto updateUserInfo(User user) throws Exception {

		UserDto userDto = new UserDto();

		if(!"".contentEquals(user.getPassword())) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			user.setPassword(passwordEncoder.encode(user.getPassword()));
		}

		int updateCnt = userMapper.updateUserInfo(user);

		if (updateCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}

	public UserDto getUserId(String email) throws Exception {

		UserDto userDto = new UserDto();

		userDto = userMapper.getUserId(email);

		return userDto;
	}

	/** 사용자 - 등록 */
	public UserDto insertUser(UserForm userForm) throws Exception {

		UserDto userDto = new UserDto();

		int insertCnt = 0;

		userForm.setAuthority("ROLE_USER");

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		userForm.setPassword(passwordEncoder.encode(userForm.getPassword()));

		insertCnt = userMapper.insertUser(userForm);

		if (insertCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}

	/** 사용자 - 단건 삭제 */
	public UserDto deleteMultiUser(UserForm userForm) throws Exception {

		UserDto userDto = new UserDto();
		Gson gson = new Gson();
		System.out.println("UserService.deleteMultiUser() ======= " + gson.toJson(userForm));
		System.out.println("userForm.getUserNoArr().length ======= " + userForm.getUserNoArr().length);

		int deleteCnt = 0;

		if(userForm.getUserNoArr() != null) {
			for(int i=0; i < userForm.getUserNoArr().length; i++) {
				userForm.setUserNo(userForm.getUserNoArr()[i]);
				deleteCnt = userMapper.deleteUser(userForm);
			}
		}

		if (deleteCnt > 0) {
			userDto.setResult("SUCCESS");
		} else {
			userDto.setResult("FAIL");
		}

		return userDto;
	}

	/** 계정 프로필 - status update - 2019. 12. 17 - LYJ
	 *              status update에 따른 API 계정 상태 update 추가 - 2020. 12. 27 - YGE
	 **/
	public ResponseEntity<Map<String, String>> updateUserStatus(UserForm userForm) {

		Map<String, String> resultMap;
		UserDto userDto = userMapper.getUserDetail(userForm.getEmail());
		String apiId;

		if(userDto == null) {
			resultMap = setResultMap("1", "There is no information about user " + userForm.getEmail());
			return new ResponseEntity<>(resultMap, HttpStatus.FORBIDDEN);
		}

		UserStatus prevStatus = UserStatus.getStatus(userDto.getStatus());
		UserStatus chgStatus = UserStatus.valueOf(userForm.getSearch_status());
		logger.info("prev : " + prevStatus + ", chg : " + chgStatus);

		apiId = userDto.getApiId();

		if(prevStatus.getApiStatus().equals("ACTIVATE") && !prevStatus.getApiStatus().equals(chgStatus.getApiStatus())){
			logger.debug("FREE/SUBSCRIBED/SUBSCRIBING --> INIT/UNSUBSCRIBED :: pause API & update status");
			if(apiId == null || apiId.equals("")){
				logger.error("User apiId is not exist so, can't pause API account.");
				resultMap = setResultMap("1", "User apiId is not exist so, can't pause API account.");
				return new ResponseEntity<>(resultMap, HttpStatus.UNPROCESSABLE_ENTITY);
			}else{
				resultMap = apiKeyService.pauseApiId(apiId);
			}
		}else if(chgStatus.getApiStatus().equals("ACTIVATE") && !prevStatus.getApiStatus().equals(chgStatus.getApiStatus())){
			logger.debug("INIT/UNSUBSCRIBED --> FREE/SUBSCRIBED/SUBSCRIBING :: create/resume API & update status");
			if(apiId == null || apiId.equals("")){
				resultMap = apiKeyService.createAPI(userDto);
			}else{
				resultMap = apiKeyService.resumeApiAccount(apiId);
			}
		}else{
			resultMap = null;
		}


		if(resultMap != null && resultMap.get("responseStatus").equals("1")){
			logger.error("Update user status fail");
			return new ResponseEntity<>(resultMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}


		if(userMapper.updateUserStatus(userForm) > 0){ //  update user status
			resultMap = setResultMap("0", "Update status success.");
			return new ResponseEntity<>(resultMap, HttpStatus.OK) ;
		}else{
			logger.error("API communication success. But update user status DB fail");
			resultMap = setResultMap("1", "API communication success. But update user status DB fail");
			return new ResponseEntity<>(resultMap, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public Map<String, String> setResultMap(String status, String msg){
		Map<String, String> map = new HashMap<>();
		map.put("responseStatus", status);
		map.put("message", msg);
		return map;
	}



	/** 계정 프로필 - 서비스 기간 update - 2019. 12. 19 - LYJ */
	public int updateUserServicePeriod(UserForm userForm) {

		Gson gson = new Gson();

		logger.info("UserService.updateUserServicePeriod() : {}", gson.toJson(userForm));

		int result = userMapper.updateUserServicePeriod(userForm);
		if(result > 0) {
			logger.info(" @ Success! UserService.updateUserServicePeriod() : {}", gson.toJson(userForm));
		} else {
			logger.info(" @ Fail! UserService.updateUserServicePeriod() : {}", gson.toJson(userForm));
		}

		return result;
	}

	/** 계정 프로필 - payment(결제방식) update - 2019. 12. 19 - LYJ */
	public int updatePayment(UserForm userForm) {

		Gson gson = new Gson();

		logger.info("UserService.updatePayment() : {}", gson.toJson(userForm));

		int result = userMapper.updatePayment(userForm);
		if(result > 0) {
			logger.info(" @ Success! UserService.updatePayment() : {}", gson.toJson(userForm));
		} else {
			logger.info(" @ Fail! UserService.updatePayment() : {}", gson.toJson(userForm));
		}

		return result;
	}

	/** 계정 프로필 - MarketingAgree update - 2019. 12. 19 - LYJ */
	public int updateMarketingAgree(UserForm userForm) {

		Gson gson = new Gson();

		logger.info("UserService.updateMarketingAgree() : {}", gson.toJson(userForm));

		int result = userMapper.updateMarketingAgree(userForm);
		if(result > 0) {
			logger.info(" @ Success! UserService.updateMarketingAgree() : {}", gson.toJson(userForm));
		} else {
			logger.info(" @ Fail! UserService.updateMarketingAgree() : {}", gson.toJson(userForm));
		}

		return result;
	}

	/** 계정 프로필 - NextPayment(다음 결제일) update - 2019. 12. 20 - LYJ */
	public int updateNextPayment(UserForm userForm) {

		Gson gson = new Gson();

		logger.info("UserService.updateNextPayment() : {}", gson.toJson(userForm));

		int resultNextPayment = userMapper.updateNextPayment(userForm);
		if(resultNextPayment > 0) {
			logger.info(" @ Success! UserService.updateNextPayment() : {}", gson.toJson(userForm));
		} else {
			logger.info(" @ Fail! UserService.updateNextPayment() : {}", gson.toJson(userForm));
		}

		// 다음 결제 일자 자체가 billing_t의 paymentDate와 createDate를 동시에 조회 -> createDate의 일자도 같이 변경 필요
		String originCreateDate = userMapper.getCreateDate(userForm);
		String changeDate = userForm.getSearch_nextPaymentDate().substring(8,10);
		String newCreateDate = originCreateDate.substring(0,8) + changeDate + originCreateDate.substring(10);

		userForm.setCreateDate(newCreateDate);
		int resultModifyCreateDate = userMapper.updateCreateDate(userForm);
		if(resultNextPayment > 0) {
			logger.info(" @ Success! UserService.updateCreateDate() : {}", gson.toJson(userForm));
		} else {
			logger.info(" @ Fail! UserService.updateCreateDate() : {}", gson.toJson(userForm));
		}

		int result;
		if(resultNextPayment == 1 && resultModifyCreateDate == 1)
			result = 1;
		else
			result = 0;

		return result;
	}

	/** 계정 프로필 - decrease payCount update - 2020. 01. 21 - LYJ */
	public int decreasePayCount(UserForm userForm) {
		Gson gson = new Gson();

		logger.info("UserService.decreasePayCount() : {}", gson.toJson(userForm));

		int result = userMapper.decreasePayCount(userForm);
		if(result > 0) {
			logger.info(" @ Success! UserService.decreasePayCount() : {}", gson.toJson(userForm));
		} else {
			logger.info(" @ Fail! UserService.decreasePayCount() : {}", gson.toJson(userForm));
		}

		return result;
	}

	/** 사용자 결제 정보 cash로 변경 - 2020. 11. 27 - LYJ */
	public int changePaymentCash(UserForm userForm) throws Exception {
		Gson gson = new Gson();

		logger.info("UserService.changePaymentCash() : {}", gson.toJson(userForm));
		UserDto userDto = userMapper.getUserDetail(userForm.getEmail());
		String email = userDto.getEmail();
		int userNo = userDto.getUserNo();

		// 기존 billing info 존재 시, 삭제
		Pay userPay = userMapper.getUserBillingInfo(userForm.getUserNo());
		if(userPay != null) {
			int deleteResult = userMapper.deleteBillingInfo(userNo);
			if(deleteResult > 0) {
				logger.info(" @ Success! UserService.changePaymentCash() - deleteBillingInfo --> userNo: {} / email: {}", userNo, email);
			} else {
				logger.info(" @ Fail! UserService.changePaymentCash() - deleteBillingInfo --> userNo: {} / email: {}", userNo, email);
				return 0;
			}
		}

		// 새로운 billing info 추가
		int insertResult = userMapper.insertBillingInfo(userNo);
		if(insertResult > 0) {
			logger.info(" @ Success! UserService.changePaymentCash() - insertBillingInfo --> userNo: {} / email: {}", userNo, email);
		} else {
			logger.info(" @ Fail! UserService.changePaymentCash() - insertBillingInfo --> userNo: {} / email: {}", userNo, email);
		}

		// 마케팅 정보 동의 설정
		int marketingAgree = Optional.ofNullable(userDto.getMarketingAgree()).orElse(0); // null인 경우 0으로 반환
		if(marketingAgree != 1) {
			int updateMarketingAgree = userMapper.updateAgreeMarketingAgree(userNo);

			if(updateMarketingAgree > 0)
				logger.info(" @ Success! UserService.changePaymentCash() - update marketingAgree --> userNo: {} / email: {}", userNo, email);
			else
				logger.info(" @ Fail! UserService.changePaymentCash() - update marketingAgree --> userNo: {} / email: {}", userNo, email);
		}

		return insertResult;
	}
}
